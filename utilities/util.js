export class Util {
  static getDistance(hexA, hexB) {
    const a = this.convertMyCoordsToOffsetToCube(hexA)
    const b = this.convertMyCoordsToOffsetToCube(hexB)
    return (Math.abs(a.x - b.x) + Math.abs(a.y - b.y) + Math.abs(a.z - b.z)) / 2
  }

  static convertMyCoordsToOffsetToCube(hex) {
    let offsetX = hex.x * 2
    if  (hex.y % 2 != 0) offsetX++
    const offsetY = Math.floor(hex.y / 2)
    const x = offsetX
    const z = offsetY - (offsetX - (offsetX&1)) / 2
    const y = -x-z
    return { x, y, z }
  }

  static lerp(a, b, t) { return a + (b - a) * t }

  static cube_lerp(a, b, t) { return { x: this.lerp(a.x, b.x, t), y: this.lerp(a.y, b.y, t), z: this.lerp(a.z, b.z, t) } }

  static hexesInLine(hexA, hexB) {
    const distance = this.getDistance(hexA, hexB)
    const a = this.convertMyCoordsToOffsetToCube(hexA)
    const b = this.convertMyCoordsToOffsetToCube(hexB)
    const results = [], results2 = []
    for (let i = 0; i <= distance; i++) {
      const q = this.cube_lerp(a, b, (1.0/distance) * i).x
      let r = this.cube_lerp(a, b, (1.0/distance) * i).z
      if (q % 0.5 == 0 && q % 1 != 0) r-=0.001  // Solves weird rounding/conversion issue
      const hex = {}
      hex.x = Math.floor(Math.round(q)/2)
      hex.y = Math.round(q) % 2 == 0 ? (Math.round(r) + hex.x) * 2 : (Math.round(r)*2) + Math.round(q)
      results.push(hex)
    }
    return results
  }

  static getNeighborHexes(hex) {
    const s = { x: hex.x, y: hex.y + 2 }
    const n = { x: hex.x, y: hex.y - 2 }
    const nw = { x: hex.x, y: hex.y - 1 }
    const sw = { x: hex.x, y: hex.y + 1 }
    const ne = { x: hex.x + 1, y: hex.y - 1 }
    const se = { x: hex.x + 1, y: hex.y + 1 }
    if (hex.y % 2 == 0) { nw.x--; sw.x--; ne.x--; se.x-- }
    return [ n, ne, se, s, sw, nw ]
  }

  static getHexesInRing(center, radius) {
    const results = []
    let current = center
    for (let i = 0; i < radius; i++) { current = this.getNeighborHexes(current)[4] }
    for (let i = 0; i < 6; i++) {
      for (let j = 0; j < radius; j++) {
        results.push(current)
        current = this.getNeighborHexes(current)[i]
      }
    }
    return results
  }

  static allTilesWithIn(loc, dist) {
    let tiles = [loc]
    for (let i = 1; i <= dist; i++) {
      tiles = tiles.concat(this.getHexesInRing(loc, i))
    }
    return tiles
  }

  static locationArrayIncludes(array, location) {
    for (let i = 0; i < array.length; i++) {
      if (this.isSameLocation(array[i], location)) return true
    }
    return false
  }

  static isSameLocation(locA, locB) { return locA.x == locB.x && locA.y == locB.y }
}

