let hackVis = false

function rgbToHex(rgb) {
  let hex = Number(rgb).toString(16)
  if (hex.length < 2) { hex = "0" + hex }
  return hex
}
function fullColorHex(r,g,b) { return "#"+rgbToHex(r)+rgbToHex(g)+rgbToHex(b) }

const spriteParams = {
  bugRanged: { xShift: 14, yShift: 14, width: 25, height: 29 },
  bugSpear: { xShift: 14, yShift: 14, width: 27, height: 28 },
  bugMain: { xShift: 12, yShift: 12, width: 24, height: 24 },
  bugMain2: { xShift: 10, yShift: 12, width: 24, height: 24 },
  iceMain: { xShift: 13, yShift: 12, width: 24, height: 24 },
  iceMain2: { xShift: 12, yShift: 12, width: 24, height: 24 },
  iceRanged: { xShift: 13, yShift: 14, width: 26, height: 28 },
  iceSpear: { xShift: 13, yShift: 16, width: 26, height: 30 },
  fightingMain: { xShift: 13, yShift: 12, width: 27, height: 26 },
  fightingMain2: { xShift: 13, yShift: 12, width: 27, height: 26 },
  fightingSpear: { xShift: 13, yShift: 15, width: 29, height: 30 },
  fightingRanged: { xShift: 13, yShift: 15, width: 29, height: 30 },
  fireMain: { xShift: 14, yShift: 16, width: 33, height: 33 },
  fireMain2: { xShift: 14, yShift: 16, width: 30, height: 30 },
  fireSpear: { xShift: 12, yShift: 14, width: 28, height: 28 },
  fireRanged: { xShift: 14, yShift: 14, width: 28, height: 28 },
  flyingMain: { xShift: 11, yShift: 11, width: 25, height: 25 },
  flyingMain2: { xShift: 11, yShift: 11, width: 25, height: 25 },
  flyingSpear: { xShift: 11, yShift: 11, width: 25, height: 25 },
  flyingRanged: { xShift: 14, yShift: 12, width: 28, height: 27 },
  grassMain: { xShift: 15, yShift: 14, width: 31, height: 31 },
  grassMain2: { xShift: 15, yShift: 12, width: 26, height: 24 },
  grassRanged: { xShift: 15, yShift: 14, width: 31, height: 31 },
  grassSpear: { xShift: 15, yShift: 14, width: 29, height: 30 },
  electricMain: { xShift: 12, yShift: 13, width: 28, height: 28 },
  electricMain2: { xShift: 15, yShift: 12, width: 28, height: 24 },
  electricSpear: { xShift: 12, yShift: 15, width: 28, height: 28 },
  electricRanged: { xShift: 13, yShift: 14, width: 28, height: 28 },
  normalMain: { xShift: 10, yShift: 12, width: 24, height: 24 },
  normalMain2: { xShift: 10, yShift: 12, width: 24, height: 24 },
  normalRanged: { xShift: 10, yShift: 12, width: 24, height: 24 },
  normalSpear: { xShift: 12, yShift: 12, width: 26, height: 28 },
  rockMain: { xShift: 12, yShift: 8, width: 26, height: 16 },
  rockMain2: { xShift: 12, yShift: 18, width: 28, height: 36 },
  rockRanged: { xShift: 15, yShift: 14, width: 32, height: 32 },
  rockSpear: { xShift: 15, yShift: 14, width: 30, height: 30 },
  waterMain: { xShift: 12, yShift: 12, width: 24, height: 24 },
  waterMain2: { xShift: 12, yShift: 12, width: 24, height: 24 },
  waterSpear: { xShift: 11, yShift: 16, width: 22, height: 31 },
  waterRanged: { xShift: 11, yShift: 11, width: 23, height: 23 },
  horseman: { xShift: 15, yShift: 14, width: 32, height: 28 },
  builder: { xShift: 11, yShift: 11, width: 23, height: 23 },
  settler: { xShift: 11, yShift: 11, width: 23, height: 23 }
}

function drawSprite(spriteName, point) {
  let params = spriteParams[spriteName]
  drawImage(spriteName, { x: point.x - params.xShift, y: point.y - params.yShift }, params.width, params.height)
}

function findIdealFontSize(str, size, maxWidth) {
  ctx.font=size+"px Arial"
  while (ctx.measureText(str).width > maxWidth) {
    size--
    ctx.font=size+"px Arial"
  }
  return size.toString()
}

function drawCharacters(characters, hexSize, charSize) {
  characters.forEach(char => {
    if (playerData.visibleTiles[char.hex.y][char.hex.x] || hackVis) {
      let point = hexIdToPixel(char.hex, hexSize)
      let spriteName = char.sprite
      if (char.sprite == 'Ranged' || char.sprite == 'Spear' || char.sprite == 'Main' || char.sprite == 'Main2') {
        spriteName = `${char.type}${spriteName}`
      }
      drawSprite(spriteName, point)
      let healthPercent = char.hp / char.maxHp
      if (healthPercent < 1.0) drawCircleBorder(point, charSize-12, fullColorHex(0,0,0), 5, healthPercent)
      let str = `${char.movement - char.moves}--${char.rangedStrength ? char.rangedStrength : char.strength}`
      drawText({ x: point.x - 14, y: point.y - 21}, str, "black", "14px Arial")
      let fSize = findIdealFontSize(char.label, 14, 45)
      let fWidth = ctx.measureText(char.label).width
      if (sCity == null) drawText({ x: point.x - (fWidth/2), y: point.y + 29}, char.label, "black", `${fSize}px Arial`)
      drawCircleBorder(point, charSize, char.team, 3, 1)
      drawCircleBorder(point, charSize-2, "black", 1, 1)
    }
  })
}

function drawLine(start, end, color, width) {
  ctx.strokeStyle = color
  ctx.lineWidth = width
  ctx.beginPath()
  ctx.moveTo(start.x, start.y)
  ctx.lineTo(end.x, end.y)
  ctx.stroke()
}

function drawText(center, text, color, font) {
  ctx.font = font
  ctx.fillStyle = color
  ctx.fillText(text, center.x, center.y)
}

function drawCircle(center, radius, color) {
  ctx.fillStyle = color
  ctx.beginPath()
  ctx.arc(center.x, center.y, radius, 0, 2 * Math.PI)
  ctx.fill()
}

function drawCircleBorder(center, radius, color, width, percent) {
  ctx.strokeStyle = color
  ctx.lineWidth = width
  ctx.beginPath()
  ctx.arc(center.x, center.y, radius, -Math.PI / 2, ((2 * Math.PI) * percent) - (Math.PI / 2))
  ctx.stroke()
}

function drawHexagon(size, x, y, color) {
  ctx.fillStyle = color
  ctx.beginPath()
  ctx.moveTo(x + size * Math.cos(0), y + size * Math.sin(0))
  for (let side = 0; side < 7; side++) {
    ctx.lineTo(x + size * Math.cos(side * 2 * Math.PI / 6), y + size * Math.sin(side * 2 * Math.PI / 6))
  }
  ctx.fill()
}

function drawLineTo(line) {
  ctx.beginPath()
  ctx.moveTo(line.start.x, line.start.y)
  ctx.lineTo(line.end.x, line.end.y)
  ctx.stroke()
}

function cartestian2Ddist(pointA, pointB) { return Math.sqrt(Math.pow(pointA.x-pointB.x,2)+Math.pow(pointA.y-pointB.y,2)) }

function pointsClose(pointA, pointB) {
  let dist = cartestian2Ddist(pointA, pointB)
  let threshold = 20
  return dist <= threshold
}

function lineClose(lineA, lineB) {
  let r_start = pointsClose(lineA.start, lineB.end)
  let r_end = pointsClose(lineA.end, lineB.start)
  return (r_start && r_end)
}


function fillInHexBorderArray(size, x, y, buffer) {
  size += buffer
  for (let side = 0; side < 6; side++) {
    let newLine = {
      start: { x: x + size * Math.cos(side * 2 * Math.PI / 6), y: y + size * Math.sin(side * 2 * Math.PI / 6) },
      end: { x: x + size * Math.cos((side+1) * 2 * Math.PI / 6), y: y + size * Math.sin((side+1) * 2 * Math.PI / 6) }
    }
    if (!isNaN(newLine.start.x) && !isNaN(newLine.start.y) && !isNaN(newLine.end.x) && !isNaN(newLine.end.y)) {
      let count = lineBorderArray.length
      lineBorderArray = lineBorderArray.filter(line => !lineClose(line, newLine))
      if (count == lineBorderArray.length) newLine.thin = false
      else newLine.thin = true
      lineBorderArray.push(JSON.parse(JSON.stringify(newLine)))
    }
  }
}

function drawHexBorder(size, x, y, color, width) {
  ctx.beginPath()
  ctx.moveTo(x + size * Math.cos(0), y + size * Math.sin(0))
  for (let side = 0; side < 7; side++) {
    ctx.lineTo(x + size * Math.cos(side * 2 * Math.PI / 6), y + size * Math.sin(side * 2 * Math.PI / 6))
  }
  ctx.strokeStyle = color
  ctx.lineWidth = width
  ctx.stroke()
}

function drawSwords(location) { drawImage("swords", { x: location.x - 20, y: location.y - 20 }, 40, 40) }

function drawImage(id, location, width, height) {
  let img = document.getElementById(id)
  ctx.drawImage(img, location.x, location.y, width, height)
}

function pixelToHexID(point, size) {
  let q = ( 2/3 * point.x ) / size
  let r = (-1/3 * point.x  +  Math.sqrt(3)/3 * point.y) / size
  let hex = {}
  hex.x = Math.floor(Math.round(q)/2)
  hex.y = Math.round(q) % 2 == 0 ? (Math.round(r) + hex.x) * 2 : (Math.round(r)*2) + Math.round(q)
  return hex
}

function hexIdToPixel(hex, size) {
  let q, r
  let point = {}
  q = hex.x * 2
  q % 2 == 0 ? r = (hex.y / 2) - hex.x : r = (hex.y - q) / 2
  point.x = size * (3/2 * q)
  point.y = size * (Math.sqrt(3)/2 * q + Math.sqrt(3) * r)
  if (hex.y % 2 != 0) point.x += size * 2 * 0.75
  return point
}

function pointInCircle(x, y, cx, cy, radius) {
  let distancesquared = (x - cx) * (x - cx) + (y - cy) * (y - cy)
  return distancesquared <= radius * radius
}

function drawHexes() {
  let center = { x: 0, y: 0 }, selectedHexCoords = { x: 0, y: 0 }
  for (let h = 0; h < resourceMap.length; h++) {
    for (let w = 0; w < resourceMap[0].length; w++) {
      let offset = h % 2 == 0 ? 0 : hexSize * 2 * 0.75
      if (playerData.visibleTiles[h][w] || hackVis) {
        let randGreen = Math.min((resourceMap[h][w].food) * 85, 255)
        let green = fullColorHex(255 - randGreen, 255, 255 - randGreen)
        let prod = Math.min(resourceMap[h][w].production, 3)
        let brown = fullColorHex(255 - (10 * prod), 255 - (prod * 26), 255 - (prod * 85))
        let color = (resourceMap[h][w].production > resourceMap[h][w].food) ? brown : green
        drawHexagon(hexSize,center.x + offset,center.y, color)
      } else { drawHexagon(hexSize, center.x + offset, center.y, "black") }
      center.x += hexSize * 3
    }
    center.x = 0
    center.y += (Math.sqrt(3) * hexSize) / 2
  }
}

function drawHexGroupRegion(hexes, team) {
  hexes.forEach(hex => {
    let coodinates = hexIdToPixel(hex, hexSize)
    ctx.globalAlpha = 0.2
    drawHexagon(hexSize+2, coodinates.x, coodinates.y, team)
    ctx.globalAlpha = 1.0
  })
}

function drawHexGroupBorder(hexes, team) {
  hexes.forEach(hex => {
    let coodinates = hexIdToPixel(hex, hexSize)
    fillInHexBorderArray(hexSize, coodinates.x, coodinates.y, 0)
  })
  ctx.strokeStyle = team
  lineBorderArray.forEach(line => { ctx.lineWidth = line.thin ? 1 : 5; drawLineTo(line) })
  lineBorderArray = []
}

function drawHoverInfo() {
  if (sChar == null) return
  let targetChar = findCharOnHex(hoverHex), targetCity = findCityOnHex(hoverHex)
  let hoverEnemey = targetChar && targetChar.team != sChar.team
  let hoverEnemeyCity = targetCity && targetCity.team != sChar.team
  if (sChar.attackRange && (hoverEnemey || hoverEnemeyCity)) {
    let coords = hexIdToPixel(sChar.hex, hexSize)
    let targetCoords = hexIdToPixel(hoverHex, hexSize)
    var headlen = 15  // length of head in pixels
    var angle = Math.atan2(targetCoords.y-coords.y,targetCoords.x-coords.x)
    ctx.beginPath()
    ctx.moveTo(coords.x, coords.y)
    ctx.lineTo(targetCoords.x, targetCoords.y)
    ctx.lineTo(targetCoords.x-headlen*Math.cos(angle-Math.PI/6),targetCoords.y-headlen*Math.sin(angle-Math.PI/6))
    ctx.moveTo(targetCoords.x, targetCoords.y)
    ctx.lineTo(targetCoords.x-headlen*Math.cos(angle+Math.PI/6),targetCoords.y-headlen*Math.sin(angle+Math.PI/6))
    ctx.strokeStyle = "red"
    ctx.lineWidth = 3
    ctx.stroke()
    return
  }
  let dist = getDistance(sChar.hex, hoverHex)
  let str = `${dist}/${sChar.movement - sChar.moves}`
  let color = (dist > (sChar.movement - sChar.moves)) ? "red" : "black"
  let targetCoords = hexIdToPixel(hoverHex, hexSize)
  if(findCharOnHex(hoverHex) && getDistance(sChar.hex, hoverHex) != 0) drawSwords(targetCoords)
  let hexes = hexesInLine(sChar.hex, hoverHex)
  hexes.forEach(hex => {
    let coodinates = hexIdToPixel(hex, hexSize)
    drawHexBorder(hexSize, coodinates.x, coodinates.y, color, 2)
  })
  drawLine(hexIdToPixel(sChar.hex, hexSize), targetCoords, color, 3)
  drawText(targetCoords, str, color, "20px Arial")
}

function drawCityFire() {
  let cityCoords = hexIdToPixel(sCity.location, hexSize)
  let targetCoords = hexIdToPixel(hoverHex, hexSize)
  var headlen = 15  // length of head in pixels
  var angle = Math.atan2(targetCoords.y-cityCoords.y,targetCoords.x-cityCoords.x)
  ctx.beginPath()
  ctx.moveTo(cityCoords.x, cityCoords.y)
  ctx.lineTo(targetCoords.x, targetCoords.y)
  ctx.lineTo(targetCoords.x-headlen*Math.cos(angle-Math.PI/6),targetCoords.y-headlen*Math.sin(angle-Math.PI/6))
  ctx.moveTo(targetCoords.x, targetCoords.y)
  ctx.lineTo(targetCoords.x-headlen*Math.cos(angle+Math.PI/6),targetCoords.y-headlen*Math.sin(angle+Math.PI/6))
  ctx.strokeStyle = "red"
  ctx.lineWidth = 3
  ctx.stroke()
}

function drawCitizens(locations) {
  locations.forEach(location => {
    if (playerData.visibleTiles[location.y][location.x] || hackVis) {
      let coordinates = hexIdToPixel(location, hexSize)
      drawCircle({ x: coordinates.x, y: coordinates.y }, citizenSize, fullColorHex(100,100,100))
      if (location == sCitizen) { drawCircleBorder({ x: coordinates.x, y: coordinates.y }, citizenSize, "black", 5, 1) }
    }
  })
}

const tileCost = { 2: 50, 3: 75 }

function drawCityResources(city) {
  let hexes = allTilesWithIn(city.location, 3)
  hexes.slice(1, hexes.length).forEach(hex => {
    let offMap = (hex.x < 0 || hex.y < 0 || hex.x >= resourceMap[0].length || hex.y >= resourceMap.length)
    if (!offMap && (playerData.visibleTiles[hex.y][hex.x] || hackVis)) {
      let coordinates = hexIdToPixel(hex, hexSize)
      let resources = resourceMap[hex.y][hex.x]
      let str = `F${resources.food}-P${resources.production}-G${resources.gold}`
      drawText({ x: coordinates.x - 20, y: coordinates.y + 27}, str, "black", "9px Arial")
      if (!hexInMyCityBorders(hex) && !hexInEnemyCityBorders(hex) && tilePurchase) {
        drawImage("dollar", { x: coordinates.x - 11, y: coordinates.y - 12 }, 24, 24)
        drawText({ x: coordinates.x - 24, y: coordinates.y - 16}, `Buy Tile: ${tileCost[hex.dist]}G`, "black", "8px Arial")
      }
    }
  })
}

function drawCities() {
  let teamTiles = {}
  allCities.forEach(city => {
    if (playerData.visibleTiles[city.location.y][city.location.x] || hackVis) {
      teamTiles[city.team] = teamTiles[city.team] ? teamTiles[city.team].concat(city.hexes) : city.hexes
      let point = hexIdToPixel(city.location, hexSize)
      drawImage("city", { x: point.x - 16, y: point.y - 16 }, 35, 35)
      let popStrenghStr = `${city.population} - ${city.strength}`
      drawText({ x: point.x - 16, y: point.y + 25}, popStrenghStr, "black", "14px Arial")
      let healthPercent = city.hp / city.maxHp
      if (healthPercent < 1.0) drawCircleBorder({ x: point.x, y: point.y - 10 }, charSize, "black", 5, healthPercent)
      if (city == sCity && !cityFireMode) {
        drawCitizens(city.citizenLocations)
        drawCityResources(sCity)
      }
    }
  })
  Object.keys(teamTiles).forEach(team => {
    drawHexGroupBorder(teamTiles[team], team)
    //drawHexGroupRegion(teamTiles[team], team)
  })
}

function drawTileImprovements() {
  let center = { x: 0, y: 0 }
  for (let h = 0; h < resourceMap.length; h++) {
    for (let w = 0; w < resourceMap[0].length; w++) {
      let offset = h % 2 == 0 ? 0 : hexSize * 2 * 0.75
      let point = pixelToHexID({ x: center.x + offset, y: center.y }, hexSize)
      if (resourceMap[point.y][point.x].improvement && (playerData.visibleTiles[point.y][point.x] || hackVis)) {
        drawImage(resourceMap[point.y][point.x].improvement, { x: center.x - 37 + offset, y: center.y - 8 }, 18, 18)
      }
      center.x += hexSize * 3
    }
    center.x = 0
    center.y += (Math.sqrt(3) * hexSize) / 2
  }
}

function drawTileResources() {
  for (let y = 0; y < resourceMap.length; y++) {
    for (let x = 0; x < resourceMap[y].length; x++) {
      let center = hexIdToPixel({x, y}, hexSize)
      if (playerData.revealedResources.includes(resourceMap[y][x].resource) && (playerData.visibleTiles[y][x] || hackVis)) {
        drawImage(resourceMap[y][x].resource, { x: center.x + 18, y: center.y - 5 }, 17, 15)
      }
    }
  }
}

function drawHuts() {
  for (let y = 0; y < resourceMap.length; y++) {
    for (let x = 0; x < resourceMap[y].length; x++) {
      let center = hexIdToPixel({x, y}, hexSize)
      if (resourceMap[y][x].tribalHut && (playerData.visibleTiles[y][x] || hackVis)) {
        drawImage("hut", { x: center.x - 14, y: center.y - 14 }, 35, 35)
      }
    }
  }
}
