$( "#cityStats" ).on( "dialogclose", ( event, ui ) => { tilePurchase = false; cityFireMode = false; sCity = null; sCitizen = null; drawMain() })
$( "#playerStats" ).on( "dialogclose", ( event, ui ) => { playerWindow = null; drawMain() })

function playerPopUp() {
  if (playerWindow == null) return
  $("#playerStats").dialog({ position: { my: "right-50 top+70", at: "right top" }, width: 500 })
  let tech =  playerData.technologyOptions.find(opt => opt.name == playerData.reseaching)
  let playerHtml = `
    Select Research <br />
  `
  playerHtml += `<select value=playerData.reseaching onchange="selectResearch(this.value)">`
  playerData.technologyOptions.forEach(opt => {
    let selected = (opt.name == playerData.reseaching) ? "selected" : ""
    let turnsToFinish = Math.max(1, Math.ceil((opt.cost - opt.progress) / playerData.sciencePerTurn ))
    playerHtml += `<option ${selected} value="${opt.name}">${opt.name} - ${turnsToFinish} turns</option>`
  })
  playerHtml += `</select> <br />`
  document.getElementById('playerText').innerHTML = playerHtml
}

function cityPopUp(city) {
  sCity = city; sChar = null; hoverHex = null
  $("#cityStats").dialog({ position: { my: "left+50", at: "left" }, width: 360 })
  document.getElementById('cityText').removeAttribute('hidden')
  let foodStatus
  if (city.foodPerTurn > 0) foodStatus = `Population Growth in ${Math.ceil((city.foodTarget-city.food) / city.foodPerTurn)}`
  else if (city.foodPerTurn < 0) foodStatus = `Starvation in ${Math.ceil((-city.food - 1) / city.foodPerTurn)}`
  else foodStatus = `City's population is Stagnating`
  let cityHtml = `
    Population: ${city.population}<br />
    Current Food: ${city.food}/${city.foodTarget}<br />
    Current Culture: ${Math.round(city.culture * 10) / 10}/${city.cultureTarget}<br />
    Food Surplus Per Turn: ${city.foodPerTurn}<br />
    Culture Per Turn: ${Math.round(city.culturePerTurn * 10) / 10}<br />
    Production Per Turn: ${city.productionPerTurn}<br />
    Border Growth in ${Math.ceil((city.cultureTarget-city.culture) / city.culturePerTurn)} Turns<br />
    Currenly Producing: ${city.producing} <br />
    ${foodStatus}<br />
    Select Production: <br />
  `
  cityHtml += `<select value=city.producing onchange="selectProduction(this.value)">`
  city.productionOptions.forEach(opt => {
    let disabled = playerData.resourceBlockedOptions.includes(opt.name) ? "disabled" : ""
    let selected = (opt.name == city.producing) ? "selected" : ""
    let turnsToBuild = Math.max(1, Math.ceil((opt.cost - opt.productionProgress) / city.productionPerTurn ))
    cityHtml += `<option ${selected} value="${opt.name}" ${disabled}>${opt.name} - ${turnsToBuild} turns</option>`
  })
  cityHtml += `</select> <br />`
  cityHtml += `Purchase Options: <br />`
  cityHtml += `<select onchange="updatePurchaseButton(city)" id="goldOption">`
  city.purchaseOptions.forEach(opt => {
    let disabled = playerData.resourceBlockedOptions.includes(opt.name) ? "disabled" : ""
    cityHtml += `<option value="${opt.name}" ${disabled}>${opt.name} - ${opt.cost} gold</option>`
  })
  cityHtml += `</select>`
  cityHtml += `<button id="purchase" disabled style="margin-left:1em" onClick='purchase()' type="button" class="btn btn-success btn-sm">Purchase</button>`
  cityHtml += `<button onclick="setTilePurchase()" type="button" class="btn btn-success btn-sm">Purchase Tile</button> <br />`
  if (city.rangedAttack) {
    cityHtml += `<button onclick="cityFire(sCity)" type="button" class="btn btn-danger btn-sm">Fire</button>`
  }
  document.getElementById('cityText').innerHTML = cityHtml
  let afford = playerData.gold >= 160
  afford ? document.getElementById("purchase").disabled = false : document.getElementById("purchase").disabled = true
}

function updatePurchaseButton() {
  let selected = sCity.purchaseOptions.find(opt => opt.name == document.getElementById('goldOption').value)
  let afford = playerData.gold >= selected.cost
  afford ? document.getElementById("purchase").disabled = false : document.getElementById("purchase").disabled = true
}

function purchase() {
  let item = document.getElementById('goldOption').value
  socket.emit('purchase', { sCity, item })
}

function cityFire(city) { cityFireMode = true; drawMain() }

function setTilePurchase() { tilePurchase = true; drawMain() }

function selectResearch(value) { socket.emit('selectResearch', { value }) }

function selectProduction(value) { socket.emit('selectProduction', { value, sCity }) }