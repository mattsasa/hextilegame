const canvas = document.getElementById("canvas")
const ctx = canvas.getContext('2d')
canvas.width = 1600; canvas.height = 1000
const hexSize = 40, charSize = 18, citizenSize = 12

let sHex = { x: 0, y: 0 }, sChar, sCity, sCitizen, myTeam, hoverHex, cityFireMode, playerWindow, tilePurchase
allCities = [], allCharacters = [], lineBorderArray = [], resourceMap = [], inPathHexes = []
playerData = { notifications: [] }

function moveAttackSelector(point) { hoverHex = pixelToHexID(point, hexSize) }

function sCharMoved() { if (sChar) sChar = allCharacters.find(char => char.id == sChar.id) }

function sCityUpdate() {
  if (sCity == null) return
  sCity = allCities.find(city => isSameLocation(city.location, sCity.location))
  if (sCity) cityPopUp(sCity)
}

function drawMain() {
  writeNotifications()
  listActions()
  drawHexes()
  if (sCity == null) drawCities()
  drawTileImprovements()
  drawTileResources()
  drawHuts()
  if (!tilePurchase) drawCharacters(allCharacters, hexSize, charSize)
  if (sCity != null) drawCities()
  if (hoverHex) cityFireMode ? drawCityFire() : drawHoverInfo()
}

function centerOnStartCity() {
  let city = allCities.find(city => city.team == myTeam)
  let coordinates = hexIdToPixel(city.location, hexSize)
  ctx.translate(-coordinates.x + 800,-coordinates.y + 200)
}

function checkIfClickInCircles(point, circleSize, array, prop) {
  for (let i = 0; i < array.length; i++) {
    let circle = prop ? array[i][prop] : array[i]
    let circleOrigin = hexIdToPixel(circle, hexSize)
    if (pointInCircle(point.x, point.y, circleOrigin.x, circleOrigin.y, circleSize)) return array[i]
  }
  return null
}

function mainMouseClick(point) {
  if (sCity != null) { cityMainClick(point); return }

  const char = checkIfClickInCircles(point, charSize, allCharacters, "hex")
  if (char != null && char.team == myTeam) { sChar = char; return }
  const city = checkIfClickInCircles(point, charSize+10, allCities, "location")
  if (city != null && city.team == myTeam) { cityPopUp(city); return }

  sHex = pixelToHexID(point, hexSize)
  sChar = null; hoverHex = null;
}


function cityMainClick(point) {
  if (tilePurchase) {
    let purchaseTiles = allTilesWithIn(sCity.location, 3).filter(tile => !hexInCityBorders(tile, sCity))
    let bought = checkIfClickInCircles(point, 13, purchaseTiles, null)
    if (bought) socket.emit('buyTile', { sCity, tile: bought })
    else tilePurchase = false
  }
  let clickedCity = checkIfClickInCircles(point, charSize+10, allCities, "location")
  if (sCity == clickedCity) {
    tilePurchase = false; cityFireMode = false; sCity = null; sCitizen = null
    $("#cityStats").dialog("close"); return
  }
  sCitizen = checkIfClickInCircles(point, citizenSize, sCity.citizenLocations, null)
  cityFireMode = false
}

function citySecondaryClick(point) {
  ///City Ranged Attack
  let clickedHex = pixelToHexID(point, hexSize)
  let clickedChar = findCharOnHex(clickedHex)
  let distance = getDistance(clickedHex, sCity.location)
  if (cityFireMode && clickedChar != null && distance <= 2) {
    cityFireMode = false; socket.emit('cityFire', { sCity, clickedChar }); return
  }
  //Move citizen if selected
  if (sCitizen == null) return
  let targetHex = pixelToHexID(point, hexSize)
  if (hexInCityBorders(targetHex, sCity)) socket.emit('moveCitizen', { src: sCitizen, dst: targetHex, sCity })
  sCitizen = null
}

function secondaryMouseClick(point) {
  if (sCity != null) { citySecondaryClick(point); return }
  if (sChar == null) return

  let clickedHex = pixelToHexID(point, hexSize)
  let clickedChar = findCharOnHex(clickedHex)
  let clickedCity = checkIfClickInCircles(point, charSize+10, allCities, "location")
  let distance = getDistance(clickedHex, sChar.hex)
  let remainingMoves = sChar.movement - sChar.moves
  //if (distance == 0 && remainingMoves > 0) socket.emit('pillage', sChar.id)
  let attacksRemain = sChar.attacks < sChar.attackSpeed
  if (clickedCity != null && clickedCity.team != myTeam) {
    if (sChar.attackRange >= distance && attacksRemain && remainingMoves > 0) {
      socket.emit('attackCity', { attacker: sChar.id, city: clickedCity, attackType: "ranged" }); return
    }
    if (distance == 1 && attacksRemain && remainingMoves > 0) {
      socket.emit('attackCity', { attacker: sChar.id, city: clickedCity, attackType: "melee" }); return
    }
  }
  else if (clickedChar != null && sChar.strength != 0 && clickedChar.team != myTeam) {
    if (sChar.attackRange >= distance && attacksRemain && remainingMoves > 0) {
      socket.emit('attackChar', { attacker: sChar.id, defender: clickedChar.id, attackType: "ranged" }); return
    }
    if (distance == 1 && attacksRemain && remainingMoves > 0) {
      socket.emit('attackChar', { attacker: sChar.id, defender: clickedChar.id, attackType: "melee" }); return
    }
  }
  else if (distance <= remainingMoves) {
    socket.emit('charMove', { source: sChar.id, destination: clickedHex })
  }
}

function improveTile(improvement) {
  socket.emit('improveTile', { id: sChar.id, improvement })
}

function updateTurn(turn) {
  if (turn != null && turn != myTeam) document.getElementById('turn').setAttribute("disabled", true)
  else document.getElementById('turn').removeAttribute("disabled")
}

function pillage() { socket.emit('pillage', sChar.id) }

function upgradeUnit() { socket.emit('upgradeUnit', sChar.id) }

function found() { socket.emit('foundCity', sChar) }

function createButtton(active, icon, buttonType, func) {
    return `<button ${active?"":"disabled"} type="button" onclick="${func}('${icon}')" class="btn btn-${buttonType} btn-sm"><img width="25" height="25" src="assets/${icon}.png"/></button>`
}

let improvements = {
  farm: true,
  mine: true,
  pasture: false
}

function listActions() {
  document.getElementById('actions').innerHTML = ""
  if (sChar == null) return
  let remainingMoves = sChar.movement - sChar.moves
  switch (sChar.sprite) {
    case "builder":
      let disableAll = resourceMap[sChar.hex.y][sChar.hex.x].improvement != null
      if (remainingMoves <= 0 || !hexInMyCityBorders(sChar.hex)) disableAll = true
      let options = { ...improvements }
      if (resourceMap[sChar.hex.y][sChar.hex.x].resource == "horse") options.pasture = true
      //resourceMap[sChar.hex.y][sChar.hex.x].food >= 2 ? options.farm = true : options.mine = true
      Object.keys(options).forEach(improvement => {
        let active = (!playerData.unlockedImprovements.includes(improvement) || disableAll) ? false : options[improvement]
        document.getElementById('actions').innerHTML += createButtton(active, improvement, "success", "improveTile")
      })
      document.getElementById('actions').innerHTML += `<p style="margin-left:1.5em">Remaining Builds: ${sChar.builds}</p>`
      break
    case "settler":
      active = remainingMoves > 0 && !anyCityInDistance(sChar.hex, 4)
      document.getElementById('actions').innerHTML += createButtton(active, "city", "primary", "found")
      break
    default:
      active = remainingMoves > 0 && hexInEnemyCityBorders(sChar.hex)
      document.getElementById('actions').innerHTML += createButtton(active, "pillage", "danger", "pillage")
      if (sChar.upgrades && playerData.unlockedOptions.includes(sChar.upgrades)) {
        let unlocked = playerData.unlockedOptions.includes(sChar.upgrades)
        let notBlocked = !playerData.resourceBlockedOptions.includes(sChar.upgrades)
        let myCity = allCities.find(city => city.team == myTeam)
        let cost = Math.round((myCity.purchaseOptions.find(opt => opt.name == sChar.upgrades).cost - myCity.purchaseOptions.find(opt => opt.name == sChar.label).cost) / 2)
        let enoughGold = playerData.gold >= cost
        let inBorders = hexInMyCityBorders(sChar.hex)
        let available = unlocked && notBlocked && enoughGold && inBorders && remainingMoves > 0
        document.getElementById('actions').innerHTML += createButtton(available, "coins", "success", "upgradeUnit")
      }
  }
}