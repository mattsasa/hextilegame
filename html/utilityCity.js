function hexInMyCityBorders(hex) {
  // for (let i = 0; i < allCities.length; i++) {
  //   if (allCities[i].team == myTeam && hexInCityBorders(hex, allCities[i])) return true
  // }
  // return false
  return allCities.reduce((bool, city) => { return (city.team == myTeam && hexInCityBorders(hex, city)) || bool }, false)
}

function hexInEnemyCityBorders(hex) {
  // for (let i = 0; i < allCities.length; i++) {
  //   if (allCities[i].team != myTeam && hexInCityBorders(hex, allCities[i])) return true
  // }
  // return false
  return allCities.reduce((bool, city) => { return (city.team != myTeam && hexInCityBorders(hex, city)) || bool }, false)
}

function hexInCityBorders(hex, city) {
  // for (let i = 0; i < city.hexes.length; i++) {
  //   if (isSameLocation(hex, city.hexes[i])) return true
  // }
  // return false
  return city.hexes.reduce((bool, loc) => { return isSameLocation(hex, loc) || bool }, false)
}

function anyCityInDistance(location, dist) {
  return allCities.reduce((bool, city) => { return getDistance(location, city.location) < dist || bool }, false)
}