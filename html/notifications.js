function writeNotifications() {
  document.getElementById('notifications').innerHTML = ""
  playerData.notifications.forEach(notification => {
    document.getElementById('notifications').innerHTML += `${notification} <br/>`
  })
  unitsWithMovement()
  document.getElementById('goldPerTurn').innerHTML = `Gold ${playerData.gold}(+${playerData.goldPerTurn})`
  document.getElementById('culturePerTurn').innerHTML = `Culture +${Math.round(playerData.culturePerTurn * 10) / 10}`
  document.getElementById('sciencePerTurn').innerHTML = `Science +${Math.round(playerData.sciencePerTurn * 10) / 10}`
}

function unitsWithMovement() {
  let remainingUnits = 0
  allCharacters.forEach(char => {
    if (char.team == myTeam && char.moves < char.movement) remainingUnits++
  })
  if (remainingUnits > 0) document.getElementById('notifications').innerHTML += `${remainingUnits} units have remaining actions`
}