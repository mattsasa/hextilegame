function getNeighborHexes(hex) {
  let s = { x: hex.x, y: hex.y + 2 }
  let n = { x: hex.x, y: hex.y - 2 }
  let nw = { x: hex.x, y: hex.y - 1 }
  let sw = { x: hex.x, y: hex.y + 1 }
  let ne = { x: hex.x + 1, y: hex.y - 1 }
  let se = { x: hex.x + 1, y: hex.y + 1 }
  if (hex.y % 2 == 0) { nw.x--; sw.x--; ne.x--; se.x-- }
  return [ n, ne, se, s, sw, nw ]
}

function convertMyCoordsToOffsetToCube(hex) {
  let offsetX = hex.x * 2
  if  (hex.y % 2 != 0) offsetX++
  let offsetY = Math.floor(hex.y / 2)
  let x = offsetX
  let z = offsetY - (offsetX - (offsetX&1)) / 2
  let y = -x-z
  return { x, y, z }
}

function getDistance(hexA, hexB) {
  let a = convertMyCoordsToOffsetToCube(hexA)
  let b = convertMyCoordsToOffsetToCube(hexB)
  return (Math.abs(a.x - b.x) + Math.abs(a.y - b.y) + Math.abs(a.z - b.z)) / 2
}

function lerp(a, b, t) { return a + (b - a) * t }

function cube_lerp(a, b, t) {
  return { x: lerp(a.x, b.x, t), y: lerp(a.y, b.y, t), z: lerp(a.z, b.z, t) }
}

function hexesInLine(hexA, hexB) {
  let distance = getDistance(hexA, hexB)
  let a = convertMyCoordsToOffsetToCube(hexA)
  let b = convertMyCoordsToOffsetToCube(hexB)
  let results = [], results2 = []
  for (let i = 0; i <= distance; i++) {
    let q = cube_lerp(a, b, (1.0/distance) * i).x
    let r = cube_lerp(a, b, (1.0/distance) * i).z
    if (q % 0.5 == 0 && q % 1 != 0) r-=0.001  // Weird rounding/conversion issue
    let hex = {}
    hex.x = Math.floor(Math.round(q)/2)
    hex.y = Math.round(q) % 2 == 0 ? (Math.round(r) + hex.x) * 2 : (Math.round(r)*2) + Math.round(q)
    results.push(hex)
  }
  return results
}


function findCharOnHex(hex) {
  for (let i = 0; i < allCharacters.length; i++) {
    if (allCharacters[i].hex.x == hex.x && allCharacters[i].hex.y == hex.y) {
      return allCharacters[i]
    }
  }
  return null
}
function findCityOnHex(hex) {
  for (let i = 0; i < allCities.length; i++) {
    if (allCities[i].location.x == hex.x && allCities[i].location.y == hex.y) {
      return allCities[i]
    }
  }
  return null
}

function checkForDeath(defender) {
  if (defender.hp <= 0) allCharacters.splice(allCharacters.indexOf(defender), 1)
}

// const typeStats = {
//   fire:  { weak: ["water","rock"], resistant: ["bug","grass","fire"] },
//   ice: { weak: ["fighting","rock","fire"], resistant: ["ice"] },
//   bug: { weak: ["flying","rock","fire"], resistant: ["fighting","grass"] },
//   fighting: { weak: ["flying"], resistant: ["bug","rock"] },
//   flying: { weak: ["rock","electric","ice"], resistant: ["fighting","bug","grass"] },
//   grass: { weak: ["flying","bug","fire","ice"], resistant: ["water","electric","grass"] },
//   electric: { weak: ["rock"], resistant: ["flying","water","electric"] },
//   normal: { weak: ["fighting"], resistant: [] },
//   rock: { weak: ["fighting","water","grass"], resistant: ["normal","flying","fire","electric"] },
//   water: { weak: ["grass","electric"], resistant: ["fire","water","ice"] }
// }

// function getAttackMultiplier(attacker, defender) {
//   let defendStats = typeStats[defender.type]
//   if (defendStats.weak.includes(attacker.type)) return 2
//   if (defendStats.resistant.includes(attacker.type)) return 0.5
//   return 1
// }

// function attackChar(defender) {
//   defender.hp -= sChar.attackDamage * getAttackMultiplier(sChar, defender)
//   sChar.attacks++
//   checkForDeath(defender)
// }

function getHexesInRing(center, radius) {
  let results = []
  let current = center
  for (let i = 0; i < radius; i++) { current = getNeighborHexes(current)[4] }
  for (let i = 0; i < 6; i++) {
    for (let j = 0; j < radius; j++) {
      current.dist = radius
      results.push(current)
      current = getNeighborHexes(current)[i]
    }
  }
  return results
}

function allTilesWithIn(loc, dist) {
  let tiles = [loc]
  for (let i = 1; i <= dist; i++) {
    tiles = tiles.concat(getHexesInRing(loc, i))
  }
  return tiles
}

function isSameLocation(locA, locB) { return (locB && locB) ? (locA.x == locB.x && locA.y == locB.y) : null }
