let socket

function connect(event) {
  const name = event.srcElement[0].value
  document.getElementById('entername').style.display = "none"
  document.getElementById('lobby').style.display = "inline"


  socket = io({ query: `name=${name}`  })

  socket.on('lobbyData', gameState => updateLobbyData(gameState))
  socket.on('initData', data => {
    document.getElementById('lobby').style.display = "none"
    document.getElementById('game').style.display = "inline"
    resourceMap = data.resourceMap; allCharacters = data.allCharacters; allCities = data.allCities; playerData = data.playerData
    myTeam = data.team; document.getElementById('teamTitle').innerHTML = `---You are Team: ${data.team.toUpperCase()}`
    document.getElementById('turn').innerHTML = `End Turn: ${data.turn}`
    sChar = allCharacters.find(char => char.team == myTeam)
    centerOnStartCity(); drawMain(); sChar = null; tilePurchase = false
  })
  socket.on('connectedPlayers', players => document.getElementById('connectedPlayers').innerHTML = players)
  socket.on('waitingFor', data => { document.getElementById('waitingForString').innerHTML = data.str; updateTurn(data.turn) })
  socket.on('turn', turn => document.getElementById('turn').innerHTML = `End Turn: ${turn}`)
  socket.on('playerData', data => { playerData = data; drawMain(); sCityUpdate(); playerPopUp() })
  socket.on('updateMap', data => { resourceMap = data; drawMain() })
  socket.on('updateUnits', units => { allCharacters = units; sCharMoved(); drawMain() })
  socket.on('updateCities', cities => { allCities = cities; sCityUpdate(); drawMain() })
}


window.onload = function(){
  trackTransforms()

  function redraw(){
    let p1 = ctx.transformedPoint(0,0)
    let p2 = ctx.transformedPoint(canvas.width,canvas.height)
    ctx.clearRect(p1.x,p1.y,p2.x-p1.x,p2.y-p1.y)
    drawMain()
  }

  let lastX=canvas.width/2, lastY=canvas.height/2
  let dragStart,dragged

  canvas.addEventListener('contextmenu', function(evt) { //Right click
    evt.preventDefault()
    secondaryMouseClick(ctx.transformedPoint(lastX,lastY))
    redraw()
  }, false)

  canvas.addEventListener('mousedown',function(evt){
    if (evt.button == 2) return
    document.body.style.mozUserSelect = document.body.style.webkitUserSelect = document.body.style.userSelect = 'none'
    lastX = evt.offsetX || (evt.pageX - canvas.offsetLeft)
    lastY = evt.offsetY || (evt.pageY - canvas.offsetTop)
    mainMouseClick(ctx.transformedPoint(lastX,lastY))
    dragStart = ctx.transformedPoint(lastX,lastY)
    dragged = false
    drawMain()
  }, false)

  canvas.addEventListener('mousemove',function(evt){
    if (sChar != null || cityFireMode) { moveAttackSelector(ctx.transformedPoint(lastX,lastY)); redraw() }
    lastX = evt.offsetX || (evt.pageX - canvas.offsetLeft)
    lastY = evt.offsetY || (evt.pageY - canvas.offsetTop)
    dragged = true
    if (dragStart){
      let pt = ctx.transformedPoint(lastX,lastY)
      ctx.translate(pt.x-dragStart.x,pt.y-dragStart.y)
      redraw()
    }
  }, false)

  canvas.addEventListener('mouseup',function(evt){
    dragStart = null
    if (!dragged) redraw()
  }, false)

  const scaleFactor = 1.05

  let zoom = function(clicks){
    let pt = ctx.transformedPoint(lastX,lastY)
    ctx.translate(pt.x,pt.y)
    let factor = Math.pow(scaleFactor,clicks)
    ctx.scale(factor,factor)
    ctx.translate(-pt.x,-pt.y)
    redraw()
  }

  let handleScroll = function(evt){
    let delta = evt.wheelDelta ? evt.wheelDelta/40 : evt.detail ? -evt.detail : 0
    if (delta) zoom(-delta)
    return evt.preventDefault() && false
  }

  canvas.addEventListener('DOMMouseScroll',handleScroll,false)
  canvas.addEventListener('mousewheel',handleScroll,false)
}

function trackTransforms(){
  let svg = document.createElementNS("http://www.w3.org/2000/svg",'svg');
  let xform = svg.createSVGMatrix()
  ctx.getTransform = function(){ return xform; }

  let savedTransforms = []
  let save = ctx.save
  ctx.save = function(){
    savedTransforms.push(xform.translate(0,0))
    return save.call(ctx)
  }

  let restore = ctx.restore
  ctx.restore = function(){
    xform = savedTransforms.pop()
    return restore.call(ctx)
  }

  let scale = ctx.scale
  ctx.scale = function(sx,sy){
    xform = xform.scaleNonUniform(sx,sy)
    return scale.call(ctx,sx,sy)
  }

  let rotate = ctx.rotate
  ctx.rotate = function(radians){
    xform = xform.rotate(radians*180/Math.PI)
    return rotate.call(ctx,radians)
  }

  let translate = ctx.translate
  ctx.translate = function(dx,dy){
    xform = xform.translate(dx,dy)
    return translate.call(ctx,dx,dy)
  }

  let transform = ctx.transform
  ctx.transform = function(a,b,c,d,e,f){
    let m2 = svg.createSVGMatrix()
    m2.a=a; m2.b=b; m2.c=c; m2.d=d; m2.e=e; m2.f=f
    xform = xform.multiply(m2)
    return transform.call(ctx,a,b,c,d,e,f)
  }

  let setTransform = ctx.setTransform
  ctx.setTransform = function(a,b,c,d,e,f){
    xform.a = a
    xform.b = b
    xform.c = c
    xform.d = d
    xform.e = e
    xform.f = f
    return setTransform.call(ctx,a,b,c,d,e,f)
  }

  let pt  = svg.createSVGPoint()
  ctx.transformedPoint = function(x,y){
    pt.x=x; pt.y=y
    return pt.matrixTransform(xform.inverse())
  }
}

function endTurnButton() { socket.emit('endTurn') }
