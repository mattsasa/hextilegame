let gameState
const availableColors = ["blue","red","yellow","green","purple","orange","silver"]

function updateLobbyData(data) {
  gameState = data
  drawEverything()
}

function drawEverything() {
  const players = []
  for (let key in gameState.socketToTeam) {
    if (gameState.socketToName[key] == undefined) continue
    let name = gameState.socketToName[key]
    players.push({ name, key, color: gameState.socketToTeam[key], ready: gameState.socketsReady[key] })
  }
  for (let key in gameState.socketToTeam) {
    if (gameState.socketToName[key] != undefined) continue
    players.push({ key, color: gameState.socketToTeam[key], ready: gameState.socketsReady[key] })
  }
  let html = ""
  const remainingColors = availableColors.filter(color => !Object.values(gameState.socketToTeam).includes(color))
  players.forEach(player => {
    html += `<div class="form-group row">`
    html += `<label class="col-sm-4 col-form-label">${ player.name ? player.name : "AI Player" }</label>`
    html += `<select class="col-sm-3 form-control form-control-sm" id="${player.key}" onchange="updateColor(this.value, this.id)"> `
    html += `<option value="${player.color}" selected>${player.color}</option>`
    remainingColors.forEach(color => { html +=  `<option value="${color}">${color}</option>` })
    html += "</select>"
    if (player.key.substring(0,2) != "AI") {
      const checkedStr = player.ready ? "checked" : ""
      const disabledStr = (socket.id == player.key) ? "" : "disabled"
      html += `<div class="col-sm-5">  <input type="checkbox" value="${player.key}" onchange="updateReady(this)" ${disabledStr} ${checkedStr}> Ready?  </div>`
    }
    html += `</div> <br>`
  })
  document.getElementById('teamColors').innerHTML = html

  const gameForm = document.getElementById('gameForm')
  gameForm.elements[0].value = gameState.numTeams
  gameForm.elements[1].value = gameState.mapDims.height
  gameForm.elements[2].value = gameState.mapDims.width
  gameForm.elements[3].checked = gameState.asyncMode
  gameForm.elements[4].checked = gameState.aiActive
  gameForm.elements[5].checked = gameState.usePokemonTypes

  document.getElementById('startButton').disabled = false
  Object.values(gameState.socketsReady).forEach(ready => {
    if (!ready) document.getElementById('startButton').disabled = true
  })
}

function updateReady(data) {
  gameState.socketsReady[data.value] = data.checked
  socket.emit('updateGameState', gameState)
  drawEverything()
}

function updateState() {
  const gameForm = new FormData(document.getElementById('gameForm'))
  if (gameForm.get('numTeams') != Object.keys(gameState.socketToTeam).length) {
    const diff = gameForm.get('numTeams') - gameState.numTeams
    gameState.numTeams = gameForm.get('numTeams')
    if (diff > 0) { // adding more AI
      for (let i = 0; i < diff; i++) {
        let aiNum = 0
        while (gameState.socketToTeam[`AI${aiNum}`] != null) aiNum++
        gameState.socketToTeam[`AI${aiNum}`] = availableColors.find(color => !Object.values(gameState.socketToTeam).includes(color))
      }
    } else { // remove some AI
      const keys = Object.keys(gameState.socketToTeam)
      let deleted = 0
      for (let i = 0; i < keys.length; i++) {
        if (keys[i].substring(0,2) == "AI") {
          delete gameState.socketToTeam[keys[i]]
          if (++deleted == -diff) break
        }
      }
    }
  }
  gameState.mapDims.height = gameForm.get('height')
  gameState.mapDims.width = gameForm.get('width')
  gameState.asyncMode = gameForm.get('asyncMode') ? true : false
  gameState.aiActive = gameForm.get('aiActive') ? true : false
  gameState.usePokemonTypes = gameForm.get('usePokemonTypes') ? true : false
  socket.emit('updateGameState', gameState)
  drawEverything()
}

function updateColor(color, key) {
  gameState.socketToTeam[key] = color
  socket.emit('updateGameState', gameState)
  drawEverything()
}
