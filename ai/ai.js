import { Util } from "../utilities/util.js"
import { Unit } from "../unit/unit.js"
import { City } from "../city/city.js"

export class AI {
  static doAiTurn(team, game) {
    const myUnits = game.allCharacters.filter(unit => unit.team == team)
    for (let i = 0; i < myUnits.length; i++) {
      if (myUnits[i].civType == "Civilian") continue
      const chosenTarget = this.chooseAttackTarget(myUnits[i], game)
      if (chosenTarget != null) {
        const dist = Util.getDistance(myUnits[i].hex, chosenTarget.hex)
        if (dist > 1) {
          this.moveToEnemy(dist, myUnits[i], chosenTarget.hex, game)
        } else {
          while (myUnits[i].attacks < myUnits[i].attackSpeed && chosenTarget != null) myUnits[i].attack(chosenTarget)
        }
        continue
      }
      const chosenPilageTile = this.choosePilageTile(myUnits[i], game)
      if (chosenPilageTile != null) {
        const dist = Util.getDistance(myUnits[i].hex, chosenPilageTile)
        if (dist >= 1) {
          this.moveToEnemy(dist, myUnits[i], chosenPilageTile, game)
        } else {
          myUnits[i].pillage()
        }
      }

    }
  }

  static moveToEnemy(dist, unit, chosenHex, game)  {
    const hexes = Util.hexesInLine(unit.hex, chosenHex)
    if (dist > 2 && unit.movement == 2 && !Unit.anyUnitsInHex(hexes[2], game) && !City.anyCityInHex(hexes[2], game)) unit.move(hexes[2])
    else if (!Unit.anyUnitsInHex(hexes[1], game)) unit.move(hexes[1])
  }

  static chooseAttackTarget(attacker, game) {
    let closestD = 9999, closestUnit = null
    game.allCharacters.forEach(unit => {
      if (unit.team != attacker.team && Util.getDistance(attacker.hex, unit.hex) < closestD) {
        closestD = Util.getDistance(attacker.hex, unit.hex); closestUnit = unit
      }
    })
    return closestUnit
  }

  static choosePilageTile(attacker, game) {
    let closestD = 9999, closestTile = null
    game.allCities.forEach(city => {
      city.hexes.slice(1, city.hexes.length).forEach(hex => {
        const bool1 = attacker.team != city.team
        const bool2 = Util.getDistance(attacker.hex, hex) < closestD
        let bool3 = true
        if (!Util.isSameLocation(hex, attacker.hex)) bool3 = !Unit.anyUnitsInHex(hex, game)
        if (bool1 && bool2 && bool3) {
          closestD = Util.getDistance(attacker.hex, hex); closestTile = hex
        }
      })
    })
    return closestTile
  }

}