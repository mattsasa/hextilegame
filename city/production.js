import { Unit } from "../unit/unit.js"

export class Production {
  static finishProd(prod, city) {
    city.game.allTeams[city.team].notifications.push("Select New Production")
    if (prod.type == "unit" || prod.type == "civilian") {
      let targetHex
      for (let i = 1; i < city.hexes.length; i++) {
        if (!Unit.anyUnitsInHex(city.hexes[i], city.game)) { targetHex = city.hexes[i]; break }
      }
      if (targetHex != null) this.spawnUnit(prod.name, targetHex, city.team, city.game)
      if (prod.name == "Settler" && city.population > 1) city.starve()
    }
    if (prod.type == "building") {
      city.centerBuildings.push(prod.name)
      city.productionOptions = city.productionOptions.filter(opt => opt.name != prod.name)
      city.purchaseOptions = city.purchaseOptions.filter(opt => opt.name != prod.name)
      if (prod.name == "Walls") { city.meleeDamage += 10; city.hp += 100; city.maxHp += 100 }
      if (prod.name == "Wonder") city.population = 100 //temp win condition
    }
    city.updateStats()
    city.player.updateStats()
  }

  static spawnUnit(name, location, team, game) {
    switch (name) {
      case "Warrior":
        game.allCharacters.push(new Unit({
          game: game,
          type: Math.floor(Math.random() * 10),
          hex: { x: location.x, y: location.y },
          sight: 2,
          movement: 2,
          attackSpeed: 1,
          hp: 100,
          strength: 20,
          maintenance: 0,
          team: team,
          sprite: "Main",
          upgrades: "Swordsman",
          label: name,
          civType: "Melee",
          era: "Ancient"
        }))
        break
      case "Swordsman":
        game.allCharacters.push(new Unit({
          game: game,
          type: Math.floor(Math.random() * 10),
          hex: { x: location.x, y: location.y },
          sight: 2,
          movement: 2,
          attackSpeed: 1,
          hp: 100,
          strength: 36,
          maintenance: 2,
          team: team,
          sprite: "Main2",
          label: name,
          civType: "Melee",
          era: "Ancient"
        }))
        break
      case "Slinger":
        game.allCharacters.push(new Unit({
          game: game,
          type: Math.floor(Math.random() * 10),
          hex: { x: location.x, y: location.y },
          sight: 2,
          movement: 2,
          attackSpeed: 1,
          attackRange: 1,
          hp: 100,
          strength: 5,
          rangedStrength: 15,
          maintenance: 0,
          team: team,
          sprite: "Ranged",
          upgrades: "Archer",
          label: name,
          civType: "Ranged",
          era: "Ancient"
        }))
        break
      case "Archer":
        game.allCharacters.push(new Unit({
          game: game,
          type: Math.floor(Math.random() * 10),
          hex: { x: location.x, y: location.y },
          sight: 2,
          movement: 2,
          attackSpeed: 1,
          attackRange: 2,
          hp: 100,
          strength: 15,
          rangedStrength: 25,
          maintenance: 1,
          team: team,
          sprite: "Ranged",
          label: name,
          civType: "Ranged",
          era: "Ancient"
        }))
        break
      case "Scout":
        game.allCharacters.push(new Unit({
          game: game,
          type: Math.floor(Math.random() * 10),
          hex: { x: location.x, y: location.y },
          sight: 2,
          movement: 3,
          attackSpeed: 1,
          hp: 100,
          strength: 10,
          maintenance: 0,
          team: team,
          sprite: "Ranged",
          label: name,
          civType: "Recon",
          era: "Ancient"
        }))
        break
      case "Spearman":
        game.allCharacters.push(new Unit({
          game: game,
          type: Math.floor(Math.random() * 10),
          hex: { x: location.x, y: location.y },
          sight: 2,
          movement: 2,
          attackSpeed: 1,
          hp: 100,
          strength: 25,
          maintenance: 1,
          team: team,
          sprite: "Spear",
          label: name,
          civType: "Anti-Cavalry",
          era: "Ancient"
        }))
        break
      case "Heavy Chariot":
        game.allCharacters.push(new Unit({
          game: game,
          type: Math.floor(Math.random() * 10),
          hex: { x: location.x, y: location.y },
          sight: 2,
          movement: 3,
          attackSpeed: 1,
          hp: 100,
          strength: 28,
          maintenance: 1,
          team: team,
          sprite: "Spear",
          label: name,
          civType: "Cavalry",
          era: "Ancient"
        }))
        break
      case "Horseman":
        game.allCharacters.push(new Unit({
          game: game,
          type: Math.floor(Math.random() * 10),
          hex: { x: location.x, y: location.y },
          sight: 2,
          movement: 4,
          attackSpeed: 1,
          hp: 100,
          strength: 36,
          maintenance: 2,
          team: team,
          sprite: "horseman",
          label: name,
          civType: "Cavalry",
          era: "Ancient"
        }))
        break
      case "Catapult":
        game.allCharacters.push(new Unit({
          game: game,
          type: Math.floor(Math.random() * 10),
          hex: { x: location.x, y: location.y },
          sight: 2,
          movement: 2,
          attackSpeed: 1,
          attackRange: 2,
          hp: 100,
          strength: 23,
          bombardStrength: 35,
          maintenance: 2,
          team: team,
          sprite: "Ranged",
          label: name,
          civType: "Siege",
          era: "Ancient"
        }))
        break
      case "Builder":
        game.allCharacters.push(new Unit({
          game: game,
          type: "none",
          hex: { x: location.x, y: location.y },
          sight: 2,
          movement: 2,
          attackSpeed: 0,
          hp: 1,
          strength: 0,
          builds: 3,
          maintenance: 0,
          team: team,
          sprite: "builder",
          civType: "Civilian",
          label: name
        }))
        break
      case "Settler":
        game.allCharacters.push(new Unit({
          game: game,
          type: "none",
          hex: { x: location.x, y: location.y },
          sight: 3,
          movement: 2,
          attackSpeed: 0,
          hp: 1,
          strength: 0,
          maintenance: 0,
          team: team,
          sprite: "settler",
          civType: "Civilian",
          label: name
        }))
    }
    return game.allCharacters[game.allCharacters.length - 1]
  }
}