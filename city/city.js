import { Util } from "../utilities/util.js"
import { Production } from "./production.js"

const culturePerPop = 0.3
const sciencePerPop = 0.5
const granaryBonus = 2
const monumentBonus = 3

const tileCost = { 2: 50, 3: 75 }

const allProdGoldOptions = [
  { name: "Warrior", prodCost: 40, goldCost: 160, type: "unit" },
  { name: "Slinger", prodCost: 35, goldCost: 140, type: "unit" },
  { name: "Builder", prodCost: 50, goldCost: 200, type: "civilian" },
  { name: "Scout", prodCost: 30, goldCost: 120, type: "unit" },
  { name: "Settler", prodCost: 80, goldCost: null, type: "civilian" },
  { name: "Spearman", prodCost: 65, goldCost: 260, type: "unit" },
  { name: "Archer", prodCost: 60, goldCost: 240, type: "unit" },
  { name: "Catapult", prodCost: 120, goldCost: 480, type: "unit" },
  { name: "Heavy Chariot", prodCost: 65, goldCost: 260, type: "unit" },
  { name: "Horseman", prodCost: 80, goldCost: 320, type: "unit" },
  { name: "Swordsman", prodCost: 90, goldCost: 360, type: "unit" },
  { name: "Monument", prodCost: 60, goldCost: 240, type: "building" },
  { name: "Granary", prodCost: 65, goldCost: 260, type: "building" },
  { name: "Water Mill", prodCost: 80, goldCost: 320, type: "building" },
  { name: "Walls", prodCost: 80, goldCost: null, type: "building" },
  { name: "Wonder", prodCost: 500, goldCost: null, type: "building" }
]

export class City {
  constructor(location, team, capital, game) {
    this.game = game
    this.player = game.allTeams[team]
    this.location = location
    this.hexes = [ { x: location.x, y: location.y } ].concat(Util.getNeighborHexes(location))
    this.capital = capital
    this.radius = 1
    this.team = team
    this.population = 1
    this.food = 0
    this.foodTarget = 0
    this.foodPerTurn = 0
    this.culture = 0
    this.cultureTarget = 0
    this.culturePerTurn = 0
    this.productionPerTurn = 0
    this.goldPerTurn = 0
    this.sciencePerTurn = 0
    this.citizenLocations = [this.hexes[4]]
    this.producing = "Warrior"
    this.productionOptions = []
    this.purchaseOptions = []
    this.centerBuildings = []
    this.rangedAttack = false
    this.rangedStrength = 40
    this.strength = 10
    this.hp = 200
    this.maxHp = 200
    this.updateStats()
    this.updateBaseStrength()

    this.player.updateVisibility(this.location, 3)
    this.player.unlockedOptions.forEach(opt => this.addProdGoldOption(opt))
  }

  updateStats() {
    this.cultureTarget = this.generateCultureTarget()
    this.culturePerTurn = (this.capital ? 1 : 0) + (this.population * culturePerPop) + (this.centerBuildings.includes("Monument") ? monumentBonus : 0)
    this.foodTarget = this.generateFoodTarget()
    this.foodPerTurn = 2 - (this.population * 2) + (this.centerBuildings.includes("Granary") ? granaryBonus : 0)
    this.productionPerTurn = (this.capital ? 2 : 0)  // Palace
    this.goldPerTurn = this.capital ? 5 : 0 // Palace
    this.sciencePerTurn = (this.capital ? 2 : 0) + (this.population * sciencePerPop)
    this.citizenLocations.forEach(tile => {
      this.foodPerTurn += (this.game.resourceMap[tile.y][tile.x].food)
      this.productionPerTurn += (this.game.resourceMap[tile.y][tile.x].production)
      this.goldPerTurn += (this.game.resourceMap[tile.y][tile.x].gold)
      this.sciencePerTurn += (this.game.resourceMap[tile.y][tile.x].science)
    })
  }

  endTurn() {

    // Citizen Yields
    this.citizenLocations.forEach(tile => {
      this.food += this.game.resourceMap[tile.y][tile.x].food
      this.getProduction().productionProgress += this.game.resourceMap[tile.y][tile.x].production
      this.player.gold += this.game.resourceMap[tile.y][tile.x].gold
      this.player.science += this.game.resourceMap[tile.y][tile.x].science
    })

    /// Culture
    let addedCulture = this.capital ? 2 : 0
    addedCulture += this.population * culturePerPop
    addedCulture += this.centerBuildings.includes("Monument") ? monumentBonus : 0
    this.culture += addedCulture; this.player.culture += addedCulture
    this.cultureTarget = this.generateCultureTarget()
    if (this.culture >= this.cultureTarget) {
      let growthCands = Util.getHexesInRing(this.location, this.radius)
      growthCands = growthCands.filter(cand => !Util.locationArrayIncludes(this.hexes, cand) && !City.hexInAnyCityBorders(cand, this.game))
      if (growthCands.length == 0) {
        growthCands = Util.getHexesInRing(this.location, ++this.radius)
        growthCands = growthCands.filter(cand => !Util.locationArrayIncludes(this.hexes, cand) && !City.hexInAnyCityBorders(cand, this.game))
      }
      this.hexes.push(growthCands.reduce((best, cand) => {
        return (best == null || this.game.resourceMap[cand.y][cand.x].food > this.game.resourceMap[best.y][best.x].food) ? cand : best
      }, null))
      this.culture -= this.cultureTarget
      this.player.updateVisibility(this.hexes[this.hexes.length - 1], 1)
    }

    /// Production
    const prod = this.getProduction()
    prod.productionProgress += this.capital ? 2 : 0
    if (prod.productionProgress >= prod.cost) {
      prod.productionProgress = 0
      this.producing = "Warrior"
      Production.finishProd(prod, this)
    }

    /// Food / Growth
    this.food -= this.population * 2
    this.food += 2 // City Center
    this.food += (this.centerBuildings.includes("Granary") ? granaryBonus : 0)
    this.foodTarget = this.generateFoodTarget()
    if (this.food >= this.foodTarget && this.population < this.hexes.length - 1) {
      this.population++
      this.food -= this.foodTarget
      let locations = this.hexes.slice(1, this.hexes.length)
      locations = locations.filter(loc => !Util.locationArrayIncludes(this.citizenLocations, loc))
      let bestFood = -1, index = -1
      locations.forEach((loc, i) => {
        if (this.game.resourceMap[loc.y][loc.x].food > bestFood) { bestFood = this.game.resourceMap[loc.y][loc.x].food; index = i }
      })
      this.citizenLocations.push(locations[index])
      this.player.notifications.push("New Population")
    }
    if (this.food < 0 && this.population > 1) this.starve()
    if (this.food < 0) this.food = 0

    /// Science
    this.player.science += this.capital ? 2 : 0
    this.player.science += this.population * sciencePerPop

    /// Gold
    this.player.gold += this.capital ? 5 : 0 // Palace

    //Reset attacks
    if (this.centerBuildings.includes("Walls")) this.rangedAttack = true

    //Heal
    this.hp += 20
    if (this.hp > this.maxHp) this.hp = this.maxHp

    this.updateStats()
    this.updateBaseStrength()
  }

  starve() {
    this.citizenLocations.splice(this.citizenLocations.length-1, 1)
    this.population--
    this.food = this.foodTarget / 2
  }

  increasePop() {
    this.population++
    let locations = this.hexes.slice(1, this.hexes.length)
    locations = locations.filter(loc => !Util.locationArrayIncludes(this.citizenLocations, loc))
    let bestFood = -1, index = -1
    locations.forEach((loc, i) => {
      if (this.game.resourceMap[loc.y][loc.x].food > bestFood) { bestFood = this.game.resourceMap[loc.y][loc.x].food; index = i }
    })
    this.citizenLocations.push(locations[index])
    this.player.notifications.push("New Population")
  }

  moveCitizen(src, dst) {
    if (this.location.x == dst.x && this.location.y == dst.y) return
    const occupied = this.citizenLocations
    let srcIndex
    for (let i = 0; i < occupied.length; i++) {
      if (occupied[i].x == dst.x && occupied[i].y == dst.y) return
      if (occupied[i].x == src.x && occupied[i].y == src.y) srcIndex = i
    }
    occupied.splice(srcIndex, 1)
    occupied.push(dst)
    this.updateStats()
  }

  buyTile(tile) {
    const adjacent = this.hexes.reduce((bool, hex) => { return Util.getDistance(hex, tile) == 1 || bool },  false)
    const cost = tileCost[Util.getDistance(tile, this.location)]
    if (cost <= this.player.gold && adjacent) {
      this.player.gold -= cost
      this.hexes.push({ x: tile.x, y: tile.y })
    }
    this.player.updateVisibility(this.hexes[this.hexes.length - 1], 1)
  }

  purchase(item) {
    const bought = this.purchaseOptions.find(opt => opt.name == item)
    this.player.gold -= bought.cost
    Production.finishProd(bought, this)
  }

  updateBaseStrength() {
    let cityStrength = Math.max(this.player.getBaseCityStrength(), 10)
    const garisonedUnit = this.game.allCharacters.find(unit => Util.isSameLocation(unit.hex, this.location))
    if (garisonedUnit && garisonedUnit.strength > cityStrength) cityStrength = garisonedUnit.strength
    if (this.centerBuildings.includes('Walls')) cityStrength += 3
    cityStrength += this.capital ? 3 : 0
    this.strength = cityStrength
    if (this.hp < 200) {
      const percent = 1 - (this.hp / 200)
      this.strength -= Math.round((2 + percent * 6))
    }
  }

  cityFire(target) {
    this.rangedStrength = Math.max(this.player.getCityRangedStrength(), 15)
    if (this.hp < 200) {
      const percent = 1 - (this.hp / 200)
      this.rangedStrength -= (2 + percent * 6)
    }
    const targetChar = this.game.allCharacters.find(char => char.id == target.id)
    targetChar.takeNeutralDamage(this.rangedStrength)
    this.rangedAttack = false
  }

  checkForDeath(attackingTeam) { if (this.hp <= 0) this.captureCity(attackingTeam) }

  captureCity(attackingTeam) {
    const startPop = this.population
    this.population = Math.ceil(this.population / 2)
    const lostPop = startPop - this.population
    this.citizenLocations.splice(0, lostPop)
    if (this.centerBuildings.includes('Walls')) {
      this.centerBuildings.splice(this.centerBuildings.indexOf('Walls'), 1)
      //this.productionOptions.push(startProductionOptions[7]) //Adding Walls as Production Option again
    }
    this.hp = Math.round(this.maxHp / 4)
    this.team = attackingTeam

    this.updateStats()
    this.updateBaseStrength()
    this.player.updateVisibility(this.location, 3)

    this.productionOptions = []
    this.purchaseOptions = []
    this.player.unlockedOptions.forEach(opt => {
      if (!this.centerBuildings.includes(opt)) this.addProdGoldOption(opt)
    })
  }

  selectProduction(newProd) {
    const theProdOpt = this.productionOptions.find(opt => opt.name == newProd)
    if (theProdOpt != null && !this.player.resourceBlockedOptions.includes(newProd)) this.producing = newProd
  }

  getProduction() { return this.productionOptions.find(option => option.name == this.producing) }

  hexInCityBorders(location) {
    for (let i = 0; i < this.hexes.length; i++) {
      if (Util.isSameLocation(location, this.hexes[i])) return true
    }
    return false
  }

  sufferPillage(loc) {
    if (this.population == 1) return
    this.citizenLocations.forEach((citizenLoc, i) => {
      if (Util.isSameLocation(citizenLoc, loc)) { this.citizenLocations.splice(i,1); this.population-- }
    })
  }

  addProdGoldOption(name) {
    const option = allProdGoldOptions.find(opt => opt.name == name)
    this.productionOptions.push({ name, cost: option.prodCost, productionProgress: 0, type: option.type })
    if (option.goldCost) this.purchaseOptions.push({ name, cost: option.goldCost, type: option.type })
  }

  generateCultureTarget() { return 10 + Math.round(Math.pow((6 * (this.hexes.length - 7)), 1.3) * 10) / 10 }
  generateFoodTarget() { return 15 + (7 * (this.population - 1)) + Math.floor(Math.pow((this.population - 1), 1.8)) }

  static getCityByLocation(location, game) {
    for (let i = 0; i < game.allCities.length; i++) {
      if (game.allCities[i].location.x == location.x && game.allCities[i].location.y == location.y) return game.allCities[i]
    }
    return false
  }

  static anyCityInHex(hex, game) {
    for (let i = 0; i < game.allCities.length; i++) {
      if (Util.isSameLocation(hex, game.allCities[i].location)) return true
    }
    return false
  }

  static hexInAnyCityBorders(hex, game) {
    for (let i = 0; i < game.allCities.length; i++) {
      if (game.allCities[i].hexInCityBorders(hex)) return game.allCities[i]
    }
    return false
  }
}