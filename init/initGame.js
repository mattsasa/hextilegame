import { City } from "../city/city.js"
import { Util } from "../utilities/util.js"
import { Player } from "../player/player.js"
import { Production } from "../city/production.js"

let huts = [], bonus = [], strat = [], game = null

function createCitiesEachChar() {
  game.allCharacters.forEach(char => {
    game.allCities.push(new City({ x: char.hex.x, y: char.hex.y + 2 }, char.team, true, game))
  })
}

function initHexes(dims) {
  for (let h = 0; h < dims.height; h++) {
    const array = []
    for (let w = 0; w < dims.width; w++) {
      array.push({
        food: Math.floor(Math.random() * 3),
        production: Math.floor(Math.random() * 3),
        gold: Math.floor(Math.random() * 3),
        culture: 0,
        science: 0
      })
    }
    game.resourceMap.push(array)
  }
}

function noOtherHutNearby(loc) {
  for (let i = 0; i < huts.length; i++) {
    if (Util.getDistance(loc, huts[i]) < 4) return false
  }
  return true
}

function noOtherResourceNearby(loc, resource, dist) {
  for (let i = 0; i < resource.length; i++) {
    if (Util.getDistance(loc, resource[i]) < dist) return false
  }
  return true
}

function generateTribalHuts() {
  for (let y = 0; y < game.resourceMap.length; y++) {
    for (let x = 0; x < game.resourceMap[y].length; x++) {
      if (noOtherHutNearby({x,y}) && !City.hexInAnyCityBorders({x,y}, game) && Math.floor(Math.random() * 20) == 0) {
        game.resourceMap[y][x].tribalHut = true; huts.push({x,y})
      }
    }
  }
}

const intToStrat = {
  0: "iron",
  1: "horse"
}

function pickStratResource() {
  return intToStrat[Math.floor(Math.random() * 2)]
}

const intToBonus = {
  0: "corn",
  1: "wheat",
  2: "copper",
  3: "stone"
}

function pickBonusResource(x, y) {
  let rand = Math.floor(Math.random() * 2)
  if (game.resourceMap[y][x].production > game.resourceMap[y][x].food) rand += 2
  return intToBonus[rand]
}

function helpUnluckyCities() {
  game.allCities.forEach(city => {
    let results = countResourcesAroundCity(city.location, 2)
    while (results.bonus + results.strat < 2) {
      addResourceToArea(results, "bonus")
    }
    results = countResourcesAroundCity(city.location, 3)
    if (results.strat == 0) addResourceToArea(results, "strat")
  })
}

function addResourceToArea(results, type) {
  const rand = Math.floor(Math.random() * results.emptyTiles.length)
  const spot = results.emptyTiles[rand]
  const newResource = type == "bonus" ? pickBonusResource(spot.x, spot.y) : pickStratResource()
  game.resourceMap[spot.y][spot.x].resource = newResource
  results.emptyTiles.splice(rand, 1)
  type == "bonus" ? results.bonus++ : results.strat
}

function countResourcesAroundCity(center, distance) {
  const area = Util.allTilesWithIn(center, distance).slice(1)
  const emptyTiles = []
  let bonus = 0, strat = 0
  area.forEach(tile => {
    let res = game.resourceMap[tile.y][tile.x].resource
    if (res) { res == "horse" || res == "iron" ? strat++ : bonus++ }
    else emptyTiles.push(tile)
  })
  return { bonus, strat, emptyTiles }
}

function generateResources() {
  for (let y = 0; y < game.resourceMap.length; y++) {
    for (let x = 0; x < game.resourceMap[y].length; x++) {
      if (City.anyCityInHex({x,y}, game)) continue
      if (Math.floor(Math.random() * 20) == 0 && noOtherResourceNearby({x,y}, strat, 2)) {
        game.resourceMap[y][x].resource = pickStratResource(x, y); strat.push({x,y})
      }
      else if (Math.floor(Math.random() * 20) == 0 && noOtherResourceNearby({x,y}, bonus, 1)) {
        game.resourceMap[y][x].resource = pickBonusResource(x, y); bonus.push({x,y})
      }
    }
  }
  helpUnluckyCities()
}

function increaseTileYields() {
  for (let y = 0; y < game.resourceMap.length; y++) {
    for (let x = 0; x < game.resourceMap[y].length; x++) {
      if (game.resourceMap[y][x].resource) {
        switch (game.resourceMap[y][x].resource) {
          case "corn": game.resourceMap[y][x].food++; break
          case "wheat": game.resourceMap[y][x].food++; break
          case "stone": game.resourceMap[y][x].production++; break
          case "copper": game.resourceMap[y][x].production++; game.resourceMap[y][x].gold++; break
          case "horse": game.resourceMap[y][x].production++; game.resourceMap[y][x].food++; break
          case "iron": game.resourceMap[y][x].science++; break
        }
      }
    }
  }
}

function initPlayers(socketToTeam) {
  Object.keys(socketToTeam).forEach(id => game.allTeams[socketToTeam[id]] = new Player(socketToTeam[id], game))
}

function spawnInitWarriors() {
  Object.keys(game.allTeams).forEach((teamColor, index) => {
    switch (index) {
      case 0: Production.spawnUnit("Warrior", { x: 8, y: 26 }, teamColor, game); break
      case 1: Production.spawnUnit("Warrior", { x: 8, y: 6 }, teamColor, game); break
      case 2: Production.spawnUnit("Warrior", { x: 12, y: 16 }, teamColor, game); break
      case 3: Production.spawnUnit("Warrior", { x: 4, y: 16 }, teamColor, game); break
      case 4: Production.spawnUnit("Warrior", { x: 16, y: 26 }, teamColor, game); break
      case 5: Production.spawnUnit("Warrior", { x: 16, y: 6 }, teamColor, game); break
      case 6: Production.spawnUnit("Warrior", { x: 20, y: 16 }, teamColor, game); break
    }
  })
}

export const initGame = (theGameData) => {
  game = theGameData
  initHexes(theGameData.mapDims)
  initPlayers(theGameData.socketToTeam)
  spawnInitWarriors()
  createCitiesEachChar()
  generateTribalHuts()
  generateResources()
  increaseTileYields()
  Object.values(game.allTeams).forEach(player => player.updateStats())
  game.humanPlayers = game.connectedSockets.length
  game.aiTeams = []
  Object.keys(game.socketToTeam).forEach(name => {
    if (name.substring(0,2) == "AI") {
      game.aiTeams.push(game.socketToTeam[name])
      delete game.socketToTeam[name]
    }
  })
  huts = [], bonus = [], strat = [], game = null
}