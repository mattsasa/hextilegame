export class Tech {
  static unlock(techname, player) {
    switch (techname) {
      case "Pottery":
        this.addOptions("Granary", player)
        break
      case "Animal Husbandry":
        player.unlockedImprovements.push("pasture")
        player.revealedResources.push("horse")
        break
      case "Mining":
        player.unlockedImprovements.push("mine")
        break
      case "Archery":
        this.addOptions("Archer", player)
        break
      case "Bronze Working":
        this.addOptions("Spearman", player)
        player.revealedResources.push("iron")
        break
      case "Iron Working":
        this.addOptions("Swordsman", player)
        break
      case "Horseback Riding":
        this.addOptions("Horseman", player)
        break
      case "Masonry":
        this.addOptions("Walls", player)
        break
      case "Wheel":
        this.addOptions("Heavy Chariot", player)
        this.addOptions("Water Mill", player)
        break
      case "Engineering":
        this.addOptions("Catapult", player)
        break
    }
  }

  static addOptions(name, player) {
    player.game.allCities.forEach(city => { if (city.team == player.team) city.addProdGoldOption(name) })
    player.unlockedOptions.push(name)
  }
}