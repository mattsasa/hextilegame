import { Util } from "../utilities/util.js"
import { Tech } from "./technology.js"

const startProdGoldOptions = ["Warrior", "Scout", "Slinger", "Builder", "Settler", "Monument", "Wonder"]

const startPlayerData = {
  endTurn: false,
  culture: 0,
  gold: 0,
  science: 0,
  scienceOverflow: 0,
  culturePerTurn: 0,
  goldPerTurn: 0,
  sciencePerTurn: 0,
  reseaching: "Pottery",
  notifications: [],
  visibleTiles: [],
  unlockedTechs: [],
  unlockedImprovements: ["farm"],
  revealedResources: ["corn","wheat","copper","stone"],
  currentStratResources: [],
  unlockedOptions: JSON.parse(JSON.stringify(startProdGoldOptions)),
  resourceBlockedOptions: [],
  technologyOptions: []
}

const resourceTypes = {
  corn: "bonus",
  wheat: "bonus",
  copper: "bonus",
  stone: "bonus",
  iron: "strat",
  horse: "strat"
}

const unitResourceRequirements = {
  Swordsman: "iron",
  Horseman: "horse"
}

const allTechnologies = [
  { name: "Pottery", prereqs: [], cost: 25, progress: 0 },
  { name: "Animal Husbandry",  prereqs: [], cost: 25, progress: 0 },
  { name: "Mining", prereqs: [], cost: 25, progress: 0 },
  { name: "Sailing", prereqs: [], cost: 50, progress: 0 },
  { name: "Astrology", prereqs: [], cost: 50, progress: 0 },
  { name: "Irrigation", prereqs: ["Pottery"], cost: 50, progress: 0 },
  { name: "Writing", prereqs: ["Pottery"], cost: 50, progress: 0 },
  { name: "Archery", prereqs: ["Animal Husbandry"], cost: 50, progress: 0 },
  { name: "Masonry", prereqs: ["Mining"], cost: 80, progress: 0 },
  { name: "Bronze Working", prereqs: ["Mining"], cost: 80, progress: 0 },
  { name: "Wheel", prereqs: ["Mining"], cost: 80, progress: 0 },
  { name: "Celestial  Navigation", prereqs: ["Sailing", "Astrology"], cost: 120, progress: 0 },
  { name: "Currency", prereqs: ["Writing"], cost: 120, progress: 0 },
  { name: "Horseback Riding", prereqs: ["Archery"], cost: 120, progress: 0 },
  { name: "Iron Working", prereqs: ["Bronze Working"], cost: 120, progress: 0 },
  { name: "Shipbuilding", prereqs: ["Sailing"], cost: 200, progress: 0 },
  { name: "Mathematics", prereqs: ["Currency"], cost: 200, progress: 0 },
  { name: "Construction", prereqs: ["Horseback Riding", "Masonry"], cost: 200, progress: 0 },
  { name: "Engineering", prereqs: ["Wheel"], cost: 200, progress: 0 }
]

export class Player {
  constructor(team, game) {
    Object.assign(this, { team, game }, JSON.parse(JSON.stringify(startPlayerData)))
    this.initVisibleTiles()
    this.addNewTechOptions()
  }

  sendPlayerData() { return Object.assign({}, this, { game: undefined }) }

  addNewTechOptions() {
    allTechnologies.forEach(tech => {
      const haveReqs = tech.prereqs.reduce((bool, req) => { return this.unlockedTechs.includes(req) && bool }, true)
      const alreadyUnlocked = this.unlockedTechs.includes(tech.name)
      const alreadyAnOption = this.technologyOptions.find(opt => opt.name == tech.name) != null
      if (haveReqs && !alreadyUnlocked && !alreadyAnOption) this.technologyOptions.push(JSON.parse(JSON.stringify(tech)))
    })
  }

  updateTech() {
    if (this.reseaching) { // Should never be null
      const tech = this.technologyOptions.find(opt => opt.name == this.reseaching)
      tech.progress += this.sciencePerTurn
      if (this.scienceOverflow > 0) { tech.progress += this.scienceOverflow; this.scienceOverflow = 0 }
      if (tech.progress >= tech.cost) {
        this.scienceOverflow = tech.progress - tech.cost
        this.unlockedTechs.push(tech.name)
        Tech.unlock(tech.name, this)
        this.technologyOptions.splice(this.technologyOptions.indexOf(tech), 1)
        this.addNewTechOptions()
        const rand = Math.floor(Math.random() * this.technologyOptions.length)
        this.reseaching = this.technologyOptions[rand].name  //Picking a random tech for user
        this.notifications.push("Select New Technology")
      }
    }
  }

  initVisibleTiles() {
    for (let h = 0; h < this.game.resourceMap.length; h++) {
      const array = []
      for (let w = 0; w < this.game.resourceMap.length[0]; w++) { array.push(false) }
      this.visibleTiles.push(array)
    }
  }

  updateVisibility(location, range) {
    const newTiles = Util.allTilesWithIn(location, range)
    newTiles.forEach(tile => { if (tile.x >= 0 && tile.y >= 0 && tile.x < 20 && tile.y < 40) this.visibleTiles[tile.y][tile.x] = true })
  }

  getBaseCityStrength() {
    let bestStrength = 0
    this.game.allCharacters.forEach(unit => { if (unit.team == this.team && unit.strength > bestStrength) bestStrength = unit.strength })
    return bestStrength - 10
  }

  getCityRangedStrength() {
    let bestStrength = 0
    this.game.allCharacters.forEach(unit => { if (unit.team == this.team && unit.rangedStrength > bestStrength) bestStrength = unit.rangedStrength })
    return bestStrength
  }

  updateStats() {
    this.goldPerTurn = 0; this.sciencePerTurn = 0; this.culturePerTurn = 0
    this.game.allCities.filter(city => city.team == this.team).forEach(city => {
       this.goldPerTurn += city.goldPerTurn; this.sciencePerTurn += city.sciencePerTurn; this.culturePerTurn += city.culturePerTurn
    })
    this.game.allCharacters.forEach(unit => { if (unit.team == this.team) this.goldPerTurn -= unit.maintenance })
  }

  addResource(resource) {
    if (resourceTypes[resource] == "strat" && !this.currentStratResources.includes(resource)) {
      this.currentStratResources.push(resource)
    }
  }

  removeResource(resource) {
    // if (resourceTypes[resource] == "strat" && !thePlayer.currentStratResources.includes(resource)) {
    //   thePlayer.currentStratResources = thePlayer.currentStratResources.filter(res => res != resource)
    // }
  }

  updateOptionsFromAvailResources() {
    this.resourceBlockedOptions = []
    Object.entries(unitResourceRequirements).forEach(([unit, requirement]) => {
      if (!this.currentStratResources.includes(requirement)) this.resourceBlockedOptions.push(unit)
    })
  }

  newTurn() {
    this.updateStats()
    this.updateTech()
    this.updateOptionsFromAvailResources()
  }
}

