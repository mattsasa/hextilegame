import express from "express"
import http from "http"
import socketio from "socket.io"
import { Game } from "./game/game.js"
import { City } from "./city/city.js"
import { initGame } from "./init/initGame.js"
const app = express()
const server = http.Server(app)
const io = socketio(server)
const port = 3000

const availableColors = ["blue","red","yellow","green","purple","orange","silver"]
const newGameSettingsState = {
  socketToTeam: {
    AI0: "blue",
    AI1: "red",
    AI2: "green",
    AI3: "yellow"
  },
  socketToName: {},
  socketsReady: {},
  numTeams: 4,
  mapDims: {
    height: 40,
    width: 25
  },
  aiActive: true,
  asyncMode: true,
  usePokemonTypes: false
}

const allGames = []
const socketToGameMap = {}
let socketsNotInGame = []
let currentSettings = JSON.parse(JSON.stringify(newGameSettingsState))

app.use(express.static(__dirname  + "/html"))
server.listen(port, () => { console.log(`Listening on port ${port}!!`) })

function addPlayer(socketID, name) {
  for (let key in currentSettings.socketToTeam) {
    if (key.substring(0,2) == "AI") {
      currentSettings.socketToTeam[socketID] = currentSettings.socketToTeam[key]
      currentSettings.socketToName[socketID] = name
      currentSettings.socketsReady[socketID] = false
      delete currentSettings.socketToTeam[key]; return
    }
  }

  //All seats are filled wiith players already
  if (currentSettings.numTeams >= 7) return
  currentSettings.numTeams++
  const newColor = availableColors.find(color => !Object.values(currentSettings.socketToTeam).includes(color))
  currentSettings.socketToTeam[socketID] = newColor
  currentSettings.socketToName[socketID] = name
  currentSettings.socketsReady[socketID] = false
}

io.on("connection", async (socket) => {

  socketsNotInGame.push(socket)
  addPlayer(socket.id, socket.handshake.query.name)
  io.emit('lobbyData', currentSettings)

  socket.on("updateGameState", async (data) => {
    currentSettings = data
    io.emit('lobbyData', currentSettings)
  })

  socket.on('startGame', async (nothing) => {
    for (let ready of Object.values(currentSettings.socketsReady)) { if (!ready) return }
    socketsNotInGame = []

    const connectedSockets = Object.keys(currentSettings.socketToTeam).reduce((sockets, id) => {
      if (io.sockets.connected[id]) sockets.push(io.sockets.connected[id])
      return sockets
    }, [])
    const newGame = new Game( Object.assign(currentSettings, { connectedSockets }) )
    allGames.push(newGame)

    newGame.connectedSockets.forEach(sock => {
      socketToGameMap[sock.id] = newGame
      sock.emit('initData', {
        resourceMap: newGame.resourceMap,
        allCharacters: newGame.sendAllUnits(),
        allCities: newGame.sendAllCities(),
        team: currentSettings.socketToTeam[sock.id],
        turn: newGame.turn,
        playerData: newGame.allTeams[currentSettings.socketToTeam[sock.id]].sendPlayerData()
      })
    })

    newGame.emit('waitingFor', newGame.waitingForString())
    newGame.emit('connectedPlayers', newGame.currentPlayer)
    console.log(`Created New Game! ID: ${newGame.id} - Human Players: ${newGame.humanPlayers} - AI Players ${newGame.numTeams - newGame.humanPlayers}`)

    currentSettings = JSON.parse(JSON.stringify(newGameSettingsState))
  })

  socket.on("charMove", async (data) => {
    const game = socketToGameMap[socket.id]
    if (!game.correctTurn(socket) && game.asyncMode) return
    game.allCharacters.find(char => char.id == data.source).move(data.destination)
    game.emitAllUnits()
    game.emit('updateMap', game.resourceMap)
    game.emitPlayerData(socket.id)
  })
  socket.on("attackChar", async (data) => {
    const game = socketToGameMap[socket.id]
    if (!game.correctTurn(socket) && game.asyncMode) return
    const attacker = game.allCharacters.find(char => char.id == data.attacker)
    const defender = game.allCharacters.find(char => char.id == data.defender)
    attacker.attack(defender)
    game.emitAllUnits()
    game.emitPlayerData(socket.id)
  })
  socket.on("attackCity", async (data) => {
    const game = socketToGameMap[socket.id]
    if (!game.correctTurn(socket) && game.asyncMode) return
    const attacker = game.allCharacters.find(char => char.id == data.attacker)
    const city = City.getCityByLocation(data.city.location, game)
    attacker.attackCity(city)
    game.emitAllUnits()
    game.emitAllCities()
    game.emitPlayerData(socket.id)
  })
  socket.on("pillage", async (attacker) => {
    const game = socketToGameMap[socket.id]
    if (!game.correctTurn(socket) && game.asyncMode) return
    game.allCharacters.find(char => char.id == attacker).pillage()
    game.emitAllUnits()
    game.emitAllCities()
    game.emit('updateMap', game.resourceMap)
    //socket.emit('playerData', game.sendPlayerData(socket.id))
  })
  socket.on("upgradeUnit", async (unit) => {
    const game = socketToGameMap[socket.id]
    if (!game.correctTurn(socket) && game.asyncMode) return
    game.allCharacters.find(char => char.id == unit).upgrade()
    game.emitAllUnits()
    game.emitPlayerData(socket.id)
  })
  socket.on("improveTile", async (data) => {
    const game = socketToGameMap[socket.id]
    if (!game.correctTurn(socket) && game.asyncMode) return
    game.allCharacters.find(char => char.id == data.id).improveTile(data.improvement)
    game.emitAllUnits()
    game.emitAllCities()
    game.emit('updateMap', game.resourceMap)
    //socket.emit('playerData', game.sendPlayerData(socket.id))
  })
  socket.on('moveCitizen', async (data) => {
    const game = socketToGameMap[socket.id]
    if (!game.correctTurn(socket) && game.asyncMode) return
    City.getCityByLocation(data.sCity.location, game).moveCitizen(data.src, data.dst)
    game.getPlayer(socket.id).updateStats()
    game.emitAllCities()
    game.emitPlayerData(socket.id)
  })
  socket.on('buyTile', async (data) => {
    const game = socketToGameMap[socket.id]
    if (!game.correctTurn(socket) && game.asyncMode) return
    City.getCityByLocation(data.sCity.location, game).buyTile(data.tile)
    game.emitAllCities()
    game.emitPlayerData(socket.id)
  })
  socket.on('selectProduction', async (data) => {
    const game = socketToGameMap[socket.id]
    if (!game.correctTurn(socket) && game.asyncMode) return
    City.getCityByLocation(data.sCity.location, game).selectProduction(data.value)
    game.emitAllCities()
  })
  socket.on('selectResearch', async (data) => {
    const game = socketToGameMap[socket.id]
    if (!game.correctTurn(socket) && game.asyncMode) return
    game.getPlayer(socket.id).reseaching = data.value
    game.emitPlayerData(socket.id)
  })
  socket.on('purchase', async (data) => {
    const game = socketToGameMap[socket.id]
    if (!game.correctTurn(socket) && game.asyncMode) return
    City.getCityByLocation(data.sCity.location, game).purchase(data.item)
    game.emitAllUnits()
    game.emitAllCities()
    game.emitPlayerData(socket.id)
  })
  socket.on('foundCity', async (settler) => {
    const game = socketToGameMap[socket.id]
    if (!game.correctTurn(socket) && game.asyncMode) return
    game.allCharacters = game.allCharacters.filter(char => char.id != settler.id)
    game.allCities.push(new City({ x: settler.hex.x, y: settler.hex.y }, settler.team, false, game))
    game.emitAllUnits()
    game.emitAllCities()
    game.emitPlayerData(socket.id)
  })
  socket.on('cityFire', async (data) => {
    const game = socketToGameMap[socket.id]
    if (!game.correctTurn(socket) && game.asyncMode) return
    City.getCityByLocation(data.sCity.location, game).cityFire(data.clickedChar)
    game.emitAllUnits()
    game.emitAllCities()
  })
  socket.on('endTurn', async () => {
    const game = socketToGameMap[socket.id]
    if (!game.correctTurn(socket) && game.asyncMode) return
    if (game.asyncMode && ++game.playerTurn >= Object.values(game.socketToTeam).length) { game.newTurn(); game.playerTurn = 0 }
    else {
      game.getPlayer(socket.id).endTurn = true
      game.emit('waitingFor', game.waitingForString())
      const humanTeams = Object.values(game.socketToTeam)
      for (let i = 0; i < humanTeams.length; i++) { if (game.allTeams[humanTeams[i]].endTurn == false) return }
      game.newTurn(); humanTeams.forEach(team => game.allTeams[team].endTurn = false)
    }
    game.emit('turn', game.turn)
    game.emitAllUnits()
    game.emitAllCities()
    game.emit('waitingFor', game.waitingForString())
    game.emitAllPlayerData()
  })
  socket.on("disconnect", async () => {
    if (Object.keys(currentSettings.socketToTeam).includes(socket.id)) {
      let aiNum = 0
      while (currentSettings.socketToTeam[`AI${aiNum}`] != undefined) aiNum++
      currentSettings.socketToTeam[`AI${aiNum}`] = currentSettings.socketToTeam[socket.id]
      delete currentSettings.socketToTeam[socket.id]
      delete currentSettings.socketToName[socket.id]
      delete currentSettings.socketsReady[socket.id]
      io.emit('lobbyData', currentSettings)
      return
    }
    delete socketToGameMap[socket.id].socketToTeam[socket.id]
    delete socketToGameMap[socket.id]
  })

})
