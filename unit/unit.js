import shortid from "shortid"
import { Util } from "../utilities/util.js"
import { City } from "../city/city.js"
import { Production } from "../city/production.js"


const intToType = {
  "none": "none",
  0: "bug",
  1: "ice",
  2: "fighting",
  3: "fire",
  4: "flying",
  5: "grass",
  6: "electric",
  7: "normal",
  8: "rock",
  9: "water"
}

const pokeTypeStats = {
  fire:  { weak: ["water","rock"], resistant: ["bug","grass","fire"] },
  ice: { weak: ["fighting","rock","fire"], resistant: ["ice"] },
  bug: { weak: ["flying","rock","fire"], resistant: ["fighting","grass"] },
  fighting: { weak: ["flying"], resistant: ["bug","rock"] },
  flying: { weak: ["rock","electric","ice"], resistant: ["fighting","bug","grass"] },
  grass: { weak: ["flying","bug","fire","ice"], resistant: ["water","electric","grass"] },
  electric: { weak: ["rock"], resistant: ["flying","water","electric"] },
  normal: { weak: ["fighting"], resistant: [] },
  rock: { weak: ["fighting","water","grass"], resistant: ["normal","flying","fire","electric"] },
  water: { weak: ["grass","electric"], resistant: ["fire","water","ice"] }
}

const civTypeWeaknesses = {
  "Cavalry": { weak: ["Anti-Cavalry"], amount: 10 },
  "Anti-Cavalry": { weak: ["Melee"], amount: 10 },
  "Siege": { weak: ["Melee", "Anti-Cavalry", "Cavalry", "Ranged"], amount: 17 },
  "Cavalry": { weak: ["Anti-Cavalry"], amount: 10 },
  "Melee": { weak: [], amount: 0 },
  "Ranged": { weak: [], amount: 0 },
  "Recon": { weak: [], amount: 0 },
}

export class Unit {
  constructor(data) {
    this.game = data.game
    this.player = data.game.allTeams[data.team]
    this.type = intToType[data.type]
    this.civType = data.civType
    this.era = data.era
    this.label = data.label
    this.hex = data.hex
    this.sight = data.sight
    this.movement = data.movement
    this.attackSpeed = data.attackSpeed
    this.moves = 0
    this.attacks = 0
    this.maxHp = data.hp
    this.hp = data.hp
    this.strength = data.strength
    this.rangedStrength = data.rangedStrength
    this.bombardStrength = data.bombardStrength
    this.attackRange = data.attackRange
    this.builds = data.builds
    this.upgrades = data.upgrades
    this.maintenance = data.maintenance
    this.team = data.team
    this.sprite = data.sprite
    this.id = shortid.generate()

    this.player.updateVisibility(this.hex, this.sight)
  }

  move(destination) {
    if (City.anyCityInHex(destination, this.game) || Unit.anyUnitsInHex(destination, this.game)) return
    const distance = Util.getDistance(destination, this.hex)
    this.hex = destination; this.moves += distance
    if (this.moves > this.movement) this.moves = this.movement
    this.checkForTribalHut()
    this.player.updateVisibility(this.hex, this.sight)
  }

  attack(defender) {
    let attackerStrength
    if (this.civType == "Ranged") attackerStrength = this.rangedStrength
    else if (this.civType == "Siege") attackerStrength = this.bombardStrength - 17
    else attackerStrength = this.strength + this.getCivTypeModifier(defender) - defender.getCivTypeModifier(this)
    const attackerAdvantage = attackerStrength - defender.strength

    let attackerFinalDmgModifier = this.attackNoise() * this.healthDmgModifier()
    let defenderFinalDmgModifier = defender.attackNoise() * defender.healthDmgModifier()
    if (this.game.usePokemonTypes) {
      attackerFinalDmgModifier *= this.getPokeTypeMultiplier(defender)
      defenderFinalDmgModifier *= defender.getPokeTypeMultiplier(this)
    }
    defender.hp -= Math.round(30 * Math.pow( Math.E, attackerAdvantage / 25.0) * attackerFinalDmgModifier)
    if (this.attackRange == null && defender.civType != "Civilian") { // is a melee attack
      this.hp -= Math.round(30 * Math.pow( Math.E, -attackerAdvantage / 25.0) * defenderFinalDmgModifier)
    }
    const defenderLoc = defender.hex
    this.attacks++; this.moves = this.movement; defender.checkForDeath(); this.checkForDeath()
    if (this.game.allCharacters.indexOf(defender) == -1 && this.game.allCharacters.indexOf(this) != -1 && this.attackRange == null) this.move(defenderLoc)
  }

  attackCity(city) {
    city.updateBaseStrength()
    let wallsMod = 1.0
    if (city.centerBuildings.includes("Walls") && city.hp > 200) {
      if (this.civType == "Ranged") wallsMod = 0.5
      else if (this.civType == "Siege") wallsMod = 1.0
      else wallsMod = 0.85
    }

    let attackerStrength
    if (this.civType == "Ranged") attackerStrength = this.rangedStrength - 17
    else if (this.civType == "Siege") attackerStrength = this.bombardStrength
    else attackerStrength = this.strength

    const attackerAdvantage = attackerStrength - city.strength
    const attackerFinalDmgModifier = this.attackNoise() * this.healthDmgModifier() * wallsMod

    city.hp -= Math.round(30 * Math.pow( Math.E, attackerAdvantage / 25.0) * attackerFinalDmgModifier)
    if (this.attackRange == null) {  /// melee attack
      this.hp -= Math.round(30 * Math.pow( Math.E, -attackerAdvantage / 25.0) * this.attackNoise())
    }
    const defenderLoc = city.location
    this.attacks++; this.moves = this.movement; this.checkForDeath()
    if (this.attackRange == null) city.checkForDeath(this.team) //Check for City Death
    if (city.hp < 1) city.hp = 1
    city.updateBaseStrength()
  }

  attackNoise() { return (Math.random() * 40.0 + 80.0) / 100.0 }

  healthDmgModifier() { return ((this.hp / this.maxHp) * 0.5) + 0.5 }

  pillage() {
    this.game.allCities.forEach(city => {
      if(city.hexInCityBorders(this.hex) && city.team != this.team) {
        this.attacks++; this.moves = this.movement; city.sufferPillage(this.hex)
        const improvement = this.game.resourceMap[this.hex.y][this.hex.x].improvement
        this.game.resourceMap[this.hex.y][this.hex.x].improvement = null
        if (improvement == "farm") this.game.resourceMap[this.hex.y][this.hex.x].food--
        if (improvement == "mine") this.game.resourceMap[this.hex.y][this.hex.x].production--
        if (improvement == "pasture") this.game.resourceMap[this.hex.y][this.hex.x].production--
        city.updateStats()

        this.player.removeResource(this.game.resourceMap[this.hex.y][this.hex.x].resource)
        this.player.updateOptionsFromAvailResources()
        this.player.updateStats()
      }
    })
  }

  upgrade() {
    if (this.upgrades && this.player.unlockedOptions.includes(this.upgrades)) {
      const unlocked = this.player.unlockedOptions.includes(this.upgrades)
      const notBlocked = !this.player.resourceBlockedOptions.includes(this.upgrades)
      const myCity = this.game.allCities.find(city => city.team == this.team)
      const cost = Math.round((myCity.purchaseOptions.find(opt => opt.name == this.upgrades).cost - myCity.purchaseOptions.find(opt => opt.name == this.label).cost) / 2)
      const enoughGold = this.player.gold >= cost
      const inBorders = City.hexInAnyCityBorders(this.hex, this.game) && City.hexInAnyCityBorders(this.hex, this.game).team == this.team
      const remainingMoves = (this.movement - this.moves) > 0
      const available = unlocked && notBlocked && enoughGold && inBorders && remainingMoves
      if (!available) return
      this.player.gold -= cost
      const newUnit = Production.spawnUnit(this.upgrades, this.hex, this.team)
      newUnit.attacks++; newUnit.moves = newUnit.movement; newUnit.type = this.type
      this.game.allCharacters.splice(this.game.allCharacters.indexOf(this), 1)
    }
  }

  takeNeutralDamage(strength) {
    const attackerAdvantage = strength - this.strength
    this.hp -= Math.round(30 * Math.pow( Math.E, -attackerAdvantage / 25.0) * this.attackNoise())
    this.checkForDeath()
  }

  checkForTribalHut() {
    if (this.game.resourceMap[this.hex.y][this.hex.x].tribalHut) {
      this.game.resourceMap[this.hex.y][this.hex.x].tribalHut = false
      const city = this.game.allCities.find(city => city.team == this.team)
      const rand = Math.floor(Math.random() * 5)
      switch (rand) {
        case 0: //Increase Pop
          city.increasePop()
          break
        case 1: //Some Gold
          this.player.gold += 75
          break
        case 2: //New Builder
          Production.finishProd({ name: "Builder", type: "unit"}, city)
          break
        case 3: //New Warrior
          Production.finishProd({ name: "Warrior", type: "unit"}, city)
          break
        case 4: //New Scout
          Production.finishProd({ name: "Scout", type: "unit"}, city)
          break
      }
    }
  }

  endTurn() {
    this.player.goldPerTurn -= this.maintenance
    this.player.gold -= this.maintenance
    if (this.moves != this.movement) {
      this.hp += 10
      this.game.allCities.forEach(city => { if (city.hexInCityBorders(this.hex)) this.hp += (city.team == this.team) ? 5 : -5 })
    }
    if (this.hp > this.maxHp) this.hp = this.maxHp
    this.attacks = 0; this.moves = 0
  }

  getPokeTypeMultiplier(defender) {
    if (defender.type == "none") return 1
    const defendStats = pokeTypeStats[defender.type]
    if (defendStats.weak.includes(this.type)) return 2
    if (defendStats.resistant.includes(this.type)) return 0.5
    return 1
  }

  getCivTypeModifier(defender) {
    if (defender.civType == "Civilian") return 0
    const defenderWeaknesses = civTypeWeaknesses[defender.civType]
    if (defenderWeaknesses.weak.includes(this.civType)) return defenderWeaknesses.amount
    else return 0
  }

  checkForDeath() { if (this.hp <= 0) this.game.allCharacters.splice(this.game.allCharacters.indexOf(this), 1) }

  improveTile(improvement) {
    this.attacks++; this.moves = this.movement
    this.game.resourceMap[this.hex.y][this.hex.x].improvement = improvement
    if (improvement == "farm") this.game.resourceMap[this.hex.y][this.hex.x].food++
    if (improvement == "mine") this.game.resourceMap[this.hex.y][this.hex.x].production++
    if (improvement == "pasture") this.game.resourceMap[this.hex.y][this.hex.x].production++
    this.builds--
    if (this.builds == 0) this.game.allCharacters.splice(this.game.allCharacters.indexOf(this), 1)
    this.game.allCities.forEach(city => { if (city.hexInCityBorders(this.hex)) city.updateStats() })

    this.player.addResource(this.game.resourceMap[this.hex.y][this.hex.x].resource)
    this.player.updateOptionsFromAvailResources()
    this.player.updateStats()
  }

  static anyUnitsInHex(hex, game) {
    for (let i = 0; i < game.allCharacters.length; i++) {
      if (Util.isSameLocation(hex, game.allCharacters[i].hex)) return true
    }
    return false
  }

  static getUnitByLocation(location, game) {
    for (let i = 0; i < game.allCharacters.length; i++) {
      if (game.allCharacters[i].hex.x == location.x && game.allCharacters[i].hex.y == location.y) return game.allCharacters[i]
    }
    return false
  }
}

