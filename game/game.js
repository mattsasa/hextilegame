import { initGame } from "../init/initGame.js"
import { AI } from "../ai/ai.js"
import shortid from "shortid"

export class Game {
  constructor(gameSettings) {
    Object.assign(this, gameSettings)
    this.id = shortid.generate()
    this.allTeams = {}
    this.playerTurn = 0
    this.turn = 1
    this.turnsFinished = []
    this.resourceMap = []
    this.allCities = []
    this.allCharacters = []
    this.allWonders = []

    initGame(this)
  }

  waitingForString() {
    const name = Object.values(this.socketToName)[this.playerTurn]
    if (this.asyncMode) return { str: `${name.toUpperCase()}'s Turn`, turn: Object.values(this.socketToTeam)[this.playerTurn] }
    let str = "Waiting For Teams: "
    Object.keys(this.socketToName).forEach(socketID => {
      if (!this.getPlayer(socketID).endTurn) { str += this.socketToName[socketID] + ", " }
    })
    return { str }
  }

  correctTurn(socket) { return this.socketToTeam[socket.id] == Object.values(this.socketToTeam)[this.playerTurn] }

  newTurn() {
    if (this.aiActive) {
      this.aiTeams.forEach(team => AI.doAiTurn(team, this))
    }

    this.allCharacters.forEach(char => char.endTurn())
    this.allCities.forEach(city => city.endTurn())
    Object.values(this.allTeams).forEach(player => player.newTurn())
    this.turn++
  }

  sendAllUnits() {
    return this.allCharacters.map(unit => {
      return Object.assign({}, unit, { game: undefined, player: undefined })
    })
  }

  sendAllCities() {
    return this.allCities.map(city => {
      return Object.assign({}, city, { game: undefined, player: undefined })
    })
  }

  emitAllUnits() { this.connectedSockets.forEach(socket => socket.emit('updateUnits', this.sendAllUnits())) }
  emitAllCities() { this.connectedSockets.forEach(socket => socket.emit('updateCities', this.sendAllCities())) }

  getPlayer(socketID) { return this.allTeams[this.socketToTeam[socketID]] }
  sendPlayerData(socketID) { return this.allTeams[this.socketToTeam[socketID]].sendPlayerData() }
  emitPlayerData(socketID) {
    this.connectedSockets.find(socket => socket.id == socketID).emit('playerData', this.sendPlayerData(socketID))
  }
  emitAllPlayerData() {
    this.connectedSockets.forEach(socket => {
      socket.emit('playerData', this.sendPlayerData(socket.id))
      this.getPlayer(socket.id).notifications = []
    })
  }
  emit(topic, data) { this.connectedSockets.forEach(socket => socket.emit(topic, data)) }

}

